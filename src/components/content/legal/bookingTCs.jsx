import React from "react";
import BreadCrumbs from "../../common/breadCrumbs";

const BookingTCs = () => {
  return (
    <>
      <BreadCrumbs
        data={[
          { src: "#", label: "Terms And Conditions" },
          { src: "#", label: "Booking" },
        ]}
      />
      <section className="ds section_padding_70 columns_padding_5">
        <div className="container">
          <div className="row bottommargin_20">
            <div className="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 text-left">
              <h2 className="big topmargin_0">Booking Terms and Conditions</h2>
              <p className="entry-excerpt bottommargin_30">
                Last updated: November 2020
              </p>
              <h4>1. Buying a Personal Experience</h4>
              <p>
                Bellainti Pty Ltd ACN 644 531 037 (we, us, our) want your
                booking through us to be as safe, efficient and as hassle-free
                as possible. To achieve this, we created the following Booking
                Terms & Conditions (Terms) which govern your reservations and
                cancellations through our website (www.bellaint.com.au) (the
                Site).
              </p>
              <h4>2. Acceptance and variation</h4>
              <p>
                (a) By creating a booking account on the Site, you agree to be
                bound by these Terms. If you do not agree to these Terms please
                do not accept them and immediately exit this account creation
                process.
              </p>
              <p>
                (b) We reserve the right to reasonably alter, amend or withdraw
                any part of these Terms without liability upon providing notice
                of the changes to you (Change Notice).
              </p>
              <p>
                (c) Your continued use of the Site, following our issue of a
                Change Notice will constitute an automatic acceptance of any
                alteration, withdrawal or amendment made.
              </p>
              <h4>3. Incorporation</h4>
              <p>
                (a) Please read our website user terms and conditions (Website
                User Terms) which are incorporated by reference into these
                Terms.
              </p>
              <p>
                (b) Where there is any inconsistency between the Website User
                Terms and these Terms, these Terms are taken to prevail to the
                extent of that inconsistency.
              </p>
              <h4>4. Bookings</h4>
              <h4>4.1 Making a booking</h4>
              <p>In order for you to make a Booking:</p>
              <p>
                (a) you must make the warranties in clause 4.3 at the time of
                making the Booking and again each day from the date the Booking
                is made until the date of your Personal Experience;
              </p>
              <p>(b) you must create an account;</p>
              <p>
                (c) choose a date and time slot for the Personal Experience
                offered by the Girl; and
              </p>
              <p>(d) pay in full in advance.</p>
              <h4>4.2 Automated Process</h4>
              <p>
                All booking requests successfully made through our Site are
                confirmed via an automated email to the account holder’s
                nominated e-mail address with a request number. This
                confirmation does not:
              </p>
              <p>(a) constitute a tax invoice; or</p>
              <p>(b) signify acceptance of your request.</p>

              <h4>4.3 Warranties</h4>
              <p>You warrant that you:</p>
              <p>
                (a) have never committed or been convicted of any indictable
                offence or any other offences relating to:
              </p>
              <p>(i) dishonesty, such as fraud, burglary or stealing; or</p>
              <p>
                (ii) abuse, such as assault, sexual assault or stalking; and
              </p>
              <p>
                (b) are not required to inform any authorities as to your
                whereabouts as a consequence of committing assault, sexual
                assault or stalking.
              </p>

              <h4>5. Obligations during Personal Experience</h4>
              <h4>5.1 </h4>
              <h4>6. Eligibility of Girls</h4>
              <h4>6.1 </h4>
              <h4>7. Protection of Girls</h4>
              <h4>7.1 </h4>
              <h4>8. Receipts</h4>
              <p>
                (a) We will forward a valid tax invoice to your nominated email
                address following your Booking being processed.{" "}
              </p>
              <p>
                (b) If you do not receive a tax invoice from us within 3 days,
                it is your responsibility to let us know.{" "}
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default BookingTCs;
