import React from "react";
import { meta_title_suffix } from "../../config.json";
import BreadCrumbs from "../common/breadCrumbs";
import DocumentMeta from "react-document-meta";

const AboutUs = () => {
  const meta = {
    description:
      "The first escort platform of its kind, Bellainti puts your pleasure on speed dial, satisfying you at every touch as the only escort concierge platform with the modern business technology you’d expect from every other pleasure in your life.",
    title: `About Us${meta_title_suffix}`,
  };

  return (
    <>
      <DocumentMeta {...meta} />
      <BreadCrumbs data={[{ src: "#", label: "About us" }]} />
      <section class="ds section_padding_70">
        <div class="container">
          <div class="row">
            <div class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6 text-center">
              <h2 class="big margin_0">About Bellainti</h2>

              <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">
                Enter the erotic world of an evolved escort experience where
                online bookings are no longer a fantasy.
              </h2>

              <h3 class="bottommargin_50">
                <i>
                  The first escort platform of its kind, Bellainti puts your
                  pleasure on speed dial, satisfying you at every touch as the
                  only escort concierge platform with the modern business
                  technology you’d expect from every other pleasure in your
                  life.
                </i>
              </h3>

              <p>
                Created by a former professional escort with 25 years experience
                in the industry, Bellainti attracts only the most professional
                and esteemed companions, ensuring you’re in the most sensual and
                superior hands, ready to engage you in the most elevating
                experiences.
              </p>

              <p>
                With the ease and efficiency of discreet online scheduling and
                payment options, there’s no more guesswork. Free up more of your
                time for intimacy by being just one click away from getting what
                you want.
              </p>

              <p>
                Search our website to be seduced by Australia’s most beautiful
                and embodied escorts, as you get intimate with their unique
                services and individual preferences. With both automated and
                manual booking options available to each escort, be sure to
                explore the unique contact method of choice for your favourite
                finds and see just how effortless it can all be.
              </p>

              <p>
                Know what you’re looking for? Tour the upcoming escort visits to
                your city and reserve a rendezvous in advance, or arrange a
                local delight with a range of incall and outcall services. For
                the ultimate penthouse browsing experience tailored to your
                personal needs and desires, be sure to explore our Member
                opportunities.
              </p>

              <p>
                We trust you’ll enjoy indulging in our website and entering the
                ecstatic world of our one of a kind escorts. Your pleasure is
                our purpose as we bring erotic excellence into the 21st century.
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default AboutUs;
