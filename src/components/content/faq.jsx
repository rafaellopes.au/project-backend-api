import React from "react";

const Faq = () => {
  return (
    <>
      <div className="row">
        <div className="col-sm-12 text-center">
          <h3 className="big bottommargin_50">Frequently Asked Questions</h3>
        </div>

        <div className="col-sm-12">
          <div className="row vertical-tabs">
            <div className="col-sm-4">
              <ul className="nav" role="tablist">
                <li className="active">
                  <a href="#vertical-tab1" role="tab" data-toggle="tab">
                    <i className="rt-icon2-home2"></i> Consectetur
                  </a>
                </li>
                <li>
                  <a href="#vertical-tab2" role="tab" data-toggle="tab">
                    <i className="rt-icon2-light-bulb"></i> Eveniet
                  </a>
                </li>
                <li>
                  <a href="#vertical-tab3" role="tab" data-toggle="tab">
                    <i className="rt-icon2-book"></i> Dolorum
                  </a>
                </li>
                <li>
                  <a href="#vertical-tab4" role="tab" data-toggle="tab">
                    <i className="rt-icon2-cog"></i> Nolestiae
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-sm-8">
              <div className="tab-content">
                <div className="tab-pane fade in active" id="vertical-tab1">
                  <p>
                    <i className="rt-icon2-anchor"></i> Lorem ipsum dolor sit
                    amet, consectetur adipisicing elit. Error quia, ad natus
                    corrupti inventore mollitia, dolor omnis nesciunt, molestias
                    officiis eum debitis dolore. Minima magnam odit cupiditate
                    labore accusantium eaque!
                  </p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Excepturi minus tenetur sunt aspernatur vitae, corporis
                    nostrum quibusdam molestias, laudantium quia in a natus
                    facilis beatae culpa inventore quidem illo atque.
                  </p>
                </div>
                <div className="tab-pane fade" id="vertical-tab2">
                  <p>
                    <i className="rt-icon2-compass"></i> Lorem ipsum dolor sit
                    amet, consectetur adipisicing elit. Suscipit voluptate, quas
                    fugit facere possimus facilis odio delectus laborum id nobis
                    expedita vitae molestiae a. Magnam aliquid architecto magni,
                    quos omnis.
                  </p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Est, enim saepe libero iure tenetur optio nisi aliquam
                    molestias ratione magnam ab ut quod possimus hic suscipit
                    doloremque, deleniti ipsa quia!
                  </p>
                </div>
                <div className="tab-pane fade" id="vertical-tab3">
                  <p>
                    <i className="rt-icon2-laptop"></i> Lorem ipsum dolor sit
                    amet, consectetur adipisicing elit. Eaque repellat,
                    reiciendis sint officia quia iure nam! Dicta omnis ex ipsa
                    fugiat maiores, vero expedita facilis, suscipit quam
                    obcaecati veniam voluptate.
                  </p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Debitis est, dolores, ex ducimus cumque iusto ipsam odit
                    voluptatum autem error impedit obcaecati quisquam molestiae,
                    optio porro inventore nostrum deleniti cupiditate.
                  </p>
                </div>
                <div className="tab-pane fade" id="vertical-tab4">
                  <p>
                    <i className="fa fa-trophy"></i> Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. A accusantium impedit
                    asperiores illum nulla sint itaque laborum perferendis
                    deleniti quo cumque, quisquam repudiandae molestias sunt ea
                    delectus porro odio recusandae!
                  </p>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Minus omnis quod eligendi mollitia vel optio neque id,
                    assumenda! Quae at quisquam eum, dolorum ullam, maxime
                    nesciunt ex modi minus illum!
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Faq;
