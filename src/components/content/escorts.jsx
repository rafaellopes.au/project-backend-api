import React, { useState, useEffect } from "react";
import SearchComponent from "./../search/searchComponent";
import BreadCrumbs from "../common/breadCrumbs";
import PageContent from "../common/pageContent";

const Escorts = (props) => {
  const [defaultSearch] = useState({});

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <BreadCrumbs data={[{ src: "#", label: "Escorts" }]} />
      <section className="ds page_models models_square gorizontal_padding section_padding_70 columns_padding_0">
        <PageContent
          slug={props.location.pathname}
          alternativeTitle="Escorts"
        />
        <SearchComponent
          default={defaultSearch}
          getLocation={false}
          showFilters={true}
          limitPerPage={10}
        />
      </section>
    </>
  );
};

export default Escorts;
