import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import SearchComponent from "./../search/searchComponent";
import BreadCrumbs from "../common/breadCrumbs";

const Categories = (props) => {
  const { category } = useParams();
  const [defaultSearch, setDefaultSearch] = useState();

  useEffect(() => {
    const locSearch = {};
    if (category) {
      locSearch.profile_category = [category.replace(/-/g, " ")];
    }
    setDefaultSearch(locSearch);
    window.scrollTo(0, 0);
  }, [category]);

  return (
    <>
      <BreadCrumbs
        data={[
          { src: "/escorts", label: "Escorts" },
          { src: "#", label: category.replace(/-/g, " ") },
        ]}
      />
      <section className="ds page_models models_square gorizontal_padding section_padding_70 columns_padding_0">
        <div className="container" style={{ marginBottom: 0 }}>
          <div className="row">
            <div className="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 text-center">
              <h2 className="big topmargin_0">Blond Escorts</h2>
              <p>
                Consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                invidunt ut labore et dolore magna aliquyam erat, sed diam
                voluptua. At vero eos et accusam et justo duo dolores et ea
                rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
                consetetur sadipscing elitr.
              </p>
            </div>
          </div>
        </div>
        {defaultSearch ? (
          <SearchComponent
            default={defaultSearch}
            getLocation={false}
            showFilters={false}
            limitPerPage={10}
          />
        ) : (
          ""
        )}
      </section>
    </>
  );
};

export default Categories;
