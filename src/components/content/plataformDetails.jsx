import React from "react";

const PlataformDetails = () => {
  return (
    <>
      <div className="row">
        <div className="col-sm-12 text-center">
          <h3 className="bottommargin_50" style={{ fontSize: "32px" }}>
            Bellainti is the only professional escort platform providing:
          </h3>
        </div>

        <div className="col-sm-12">
          <div className="row vertical-tabs">
            <div className="col-sm-5">
              <ul className="nav" role="tablist">
                <li className="active">
                  <a href="#vertical-tab1" role="tab" data-toggle="tab">
                    Instant online booking software
                  </a>
                </li>
                <li>
                  <a href="#vertical-tab2" role="tab" data-toggle="tab">
                    Instant online payment technology
                  </a>
                </li>
                <li>
                  <a href="#vertical-tab3" role="tab" data-toggle="tab">
                    Purpose over profit
                  </a>
                </li>
                <li>
                  <a href="#vertical-tab4" role="tab" data-toggle="tab">
                    Industry specific wellness platform
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-sm-7">
              <div className="tab-content">
                <div
                  className="tab-pane fade in active"
                  id="vertical-tab1"
                  style={{ minHeight: "181px" }}
                >
                  <p>
                    Continue managing your own calendar manually, or opt to use
                    our online calendar software to take instant online bookings
                    with real time availability. Let us be your secretary whilst
                    you ditch the phone for a coffee or a surf, and wake up to
                    new bookings without lifting a finger.
                  </p>
                </div>
                <div
                  className="tab-pane fade"
                  id="vertical-tab2"
                  style={{ minHeight: "181px" }}
                >
                  <p>
                    Set your own rates, and see them instantly appear in your
                    bank account! This is a first for the industry, and puts an
                    end to the financial and technological discrimination that
                    escorts like you have long had to endure.
                  </p>
                </div>
                <div
                  className="tab-pane fade"
                  id="vertical-tab3"
                  style={{ minHeight: "181px" }}
                >
                  <p>
                    Evolution is at the forefront of Bellainti. This is no
                    standard, automated advertising platform.. It’s the
                    beginning of modern business technologies working for the
                    wellbeing of legal operating sex workers; something never
                    seen before in the industry.
                  </p>
                  <p>
                    Making money is not our primary intention; evolving the
                    industry is. If a better system or software becomes
                    available, you can be sure that we’ll implement it. We’re in
                    the game of industry expansion and we’re in it for you, for
                    the long haul.
                  </p>
                </div>
                <div
                  className="tab-pane fade"
                  id="vertical-tab4"
                  style={{ minHeight: "181px" }}
                >
                  <p>
                    With your wellbeing at the centre of our mission, fresh
                    content will drop regularly into our educational monthly
                    membership platform, designed specifically to enhance and
                    improve the lives of sex workers. From self care and pelvic
                    floor health, to business strategies and money wisdom, we
                    care for the whole you and how you thrive behind the scenes.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PlataformDetails;
