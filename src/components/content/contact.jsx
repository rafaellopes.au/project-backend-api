import React, { useState } from "react";
import Joi from "joi-browser";
import { sendEmail } from "../../services/miscellaneous";
import BreadCrumbs from "../common/breadCrumbs";
import {
  RenderInput,
  RenderTextArea,
  validateProperty,
  validate,
} from "../common/form";
import DOMPurify from "dompurify";
import DocumentMeta from "react-document-meta";
import { meta_title_suffix } from "../../config.json";

const Contact = () => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    mobile: "",
    message: "",
  });
  const [completed, setCompleted] = useState(false);
  const schema = {
    name: Joi.string().min(2).max(255).required().label("Name"),
    email: Joi.string().min(5).max(255).required().email().label("Email"),
    mobile: Joi.string().allow(null, "").min(10).max(10).label("Mobile Number"),
    message: Joi.string().min(10).max(2500).required().label("Message"),
  };
  const [error, setError] = useState({});
  const [processing, setProcessing] = useState(false);

  const handleChange = (k, v) => {
    let newData = { ...formData };
    newData[k] = v;
    setFormData(newData);
    const validate = validateProperty(k, v, schema);

    let newError = { ...error };
    newError[k] = validate;
    setError(newError);
  };

  const processSubmit = async () => {
    try {
      const message = `<h3>New Message: Contact Page</h3>
      <hr />
      <p>Name: <b>${DOMPurify.sanitize(formData.name)}</b></p>
      <p>Email: <b>${DOMPurify.sanitize(formData.email)}</b></p>
      <p>Mobile Number: <b>${DOMPurify.sanitize(formData.mobile)}</b></p>
      <p>Message:</p>
      <p>${DOMPurify.sanitize(formData.message)}</p>`;

      await sendEmail(message, "New Message: Contact Page");
      setCompleted(true);
    } catch (ex) {
      setProcessing(false);
      console.log("ex", ex);
      if (ex.response && ex.response.status === 400) {
        const errors = { ...error };
        errors.submit = ex.response.data;
        setError({ ...errors });
      } else {
        const errors = { ...error };
        errors.submit = "Internal Error!";
        setError({ ...errors });
      }
    }
  };

  const handleSubmit = (e) => {
    setProcessing(true);
    e.preventDefault();
    const errors = validate(formData, schema);
    if (errors) {
      setProcessing(false);
      return setError(errors);
    }
    processSubmit();
  };

  const meta = {
    title: `Contact Us ${meta_title_suffix}`,
    description:
      "We go wild for all the ways your time and pleasure can feel even better, so we’re always ready to hear from you and massage out any concerns.",
  };

  return (
    <>
      <DocumentMeta {...meta} />
      <BreadCrumbs data={[{ src: "#", label: "Contact us" }]} />
      <section class="ds section_padding_70">
        <div class="container">
          <div class="row">
            <div class="text-center">
              <h2 class="big margin_0">Contact Bellainti</h2>
              <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">
                Something standing in the way of your good time?
              </h2>

              <p>
                We go wild for all the ways your time and pleasure can feel even
                better, so we’re always ready to hear from you and massage out
                any concerns.
              </p>
              <p>
                <b>For our advertisers</b> - A smooth experience that saves you
                time is our game, so get in touch regarding any technical
                problems, advert approval enquiries, advertising guidelines
                questions, complaints, website feedback or to report any abuse.
                Our team is your team, on call to keep the process as smooth as
                possible for you.{" "}
              </p>
              <p>
                <b>For our members</b> - Whilst we trust you’re always in good
                hands with our world class companions, contact us here regarding
                any complaints, website feedback or to report any abuse. Your
                experience is valued, as we continue to expand and evolve the
                most curated and intimate escort booking platform on the
                internet.
              </p>

              <p>&nbsp;</p>

              <div className={completed === true ? "d-none" : "contact-form"}>
                <form
                  className=""
                  onSubmit={handleSubmit}
                  className={completed === true ? "d-none" : ""}
                >
                  <RenderInput
                    label="Name*"
                    required="true"
                    value={formData.name}
                    error={error.name}
                    name="name"
                    id="name"
                    handleChange={handleChange}
                  />

                  <RenderInput
                    label="Email*"
                    required="true"
                    value={formData.email}
                    error={error.email}
                    name="email"
                    id="email"
                    handleChange={handleChange}
                  />

                  <RenderInput
                    label="Mobile Number"
                    required="false"
                    value={formData.mobile}
                    error={error.mobile}
                    name="mobile"
                    id="mobile"
                    handleChange={handleChange}
                  />

                  <RenderTextArea
                    label="Message*"
                    required="true"
                    value={formData.message}
                    error={error.message}
                    name="message"
                    id="message"
                    type="message"
                    handleChange={handleChange}
                  />
                  {!processing ? (
                    <button
                      id="contact_form_submit"
                      name="contact_submit"
                      class="theme_button color1"
                    >
                      Start Now
                    </button>
                  ) : (
                    <button
                      id="contact_form_submit"
                      name="contact_submit"
                      disabled
                    >
                      Processing...
                    </button>
                  )}
                  {error && error.submit ? (
                    <span
                      className="alert alert-danger d-block"
                      style={{
                        padding: "1px 20px",
                        marginTop: "10px",
                        display: "block",
                      }}
                    >
                      {error.submit}
                    </span>
                  ) : (
                    ""
                  )}
                </form>

                <p>&nbsp;</p>
                <p>
                  ** For any questions or enquiries regarding specific escort
                  services, please contact the escort directly.
                </p>
              </div>
              <div className={completed === false ? "d-none" : "success-msg"}>
                <p style={{ marginBottom: "0px" }}>
                  Your message has been sent. Thank you! Our team will contact
                  you back soon.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Contact;
