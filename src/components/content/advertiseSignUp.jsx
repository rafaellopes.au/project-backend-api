import React, { useState, useEffect } from "react";
import { adminSite } from "../../config.json";
import register from "../../services/signupServices";
import Joi from "joi-browser";
import {
  RenderInput,
  RenderSelect,
  validateProperty,
  RenderCheckBox,
  validate,
} from "../common/form";
import BreadCrumbs from "../common/breadCrumbs";
import gaFire from "../common/gaFire";

const AdvertiseSignUp = () => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    mobile: "",
    password: "",
    confirmPassword: "",
    gender: "Female",
  });

  const [completed, setCompleted] = useState(false);
  const schema = {
    name: Joi.string().min(2).max(255).required().label("Name"),
    email: Joi.string().min(5).max(255).required().email().label("Email"),
    mobile: Joi.string().required().min(10).max(10).label("Mobile Number"),
    password: Joi.string().min(8).max(255).required().label("Password"),
    confirmPassword: Joi.string()
      .min(8)
      .max(255)
      .required()
      .label("Confirm Password"),
    gender: Joi.string().required().label("Gender"),
  };

  const [error, setError] = useState({});
  const [processing, setProcessing] = useState(false);

  const handleChange = (k, v) => {
    let newData = { ...formData };
    newData[k] = v;
    setFormData(newData);
    const validate = validateProperty(k, v, schema);

    let newError = { ...error };
    newError[k] = validate;
    setError(newError);
  };

  const processSubmit = async () => {
    if (formData.password !== formData.confirmPassword) {
      setError({
        submit: "Confirm password doesn't match with the password.",
      });
      setProcessing(false);
      setTimeout(() => {
        setError({});
      }, 3000);
      return null;
    }

    let format_formData = formData;
    delete format_formData.confirmPassword;
    try {
      await register(format_formData);
      setCompleted(true);
      setFormData({});
      gaFire.advertiseSignUp();
    } catch (ex) {
      setProcessing(false);
      console.log("ex", ex);
      if (ex.response && ex.response.status === 400) {
        const errors = { ...error };
        errors.submit = ex.response.data;
        setError({ ...errors });
      }
    }
  };

  const handleSubmit = (e) => {
    setProcessing(true);
    e.preventDefault();
    const errors = validate(formData, schema);
    if (errors) {
      setProcessing(false);
      return setError(errors);
    }

    processSubmit();
  };

  const redirectTo = (link) => {
    window.location.href = link;
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <BreadCrumbs
        data={[
          { src: "/advertise", label: "Advertise" },
          { src: "#", label: "Sign Up" },
        ]}
      />
      <section class="ds section_padding_70">
        <div class="container">
          <div class="row">
            <div class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6 text-center">
              <h2 class="big margin_0">Create you profile</h2>
              <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">
                Lorem ipsum dolor sit amet!
              </h2>
              <form
                className={completed === true ? "d-none" : ""}
                onSubmit={handleSubmit}
              >
                <RenderInput
                  label="Display Name"
                  required="true"
                  value={formData.name}
                  error={error.name}
                  name="name"
                  id="name"
                  handleChange={handleChange}
                />
                <RenderInput
                  label="Email"
                  required="true"
                  value={formData.email}
                  error={error.email}
                  name="email"
                  id="email"
                  handleChange={handleChange}
                />
                <RenderInput
                  label="Mobile Number"
                  value={formData.mobile}
                  error={error.mobile}
                  name="mobile"
                  id="mobile"
                  handleChange={handleChange}
                />
                <RenderInput
                  label="Password"
                  required="true"
                  type="password"
                  value={formData.password}
                  error={error.password}
                  name="password"
                  id="password"
                  handleChange={handleChange}
                />
                <RenderInput
                  label="Confirm Password"
                  required="true"
                  type="password"
                  value={formData.confirmPassword}
                  error={error.confirmPassword}
                  name="confirmPassword"
                  id="confirmPassword"
                  handleChange={handleChange}
                />
                <RenderSelect
                  label="Gender"
                  required="true"
                  options={[
                    { k: "Female", v: "Female" },
                    { k: "Male", v: "Male" },
                    { k: "Trans", v: "Trans" },
                  ]}
                  value={formData.gender}
                  name="gender"
                  id="gender"
                  handleChange={handleChange}
                />

                <RenderCheckBox
                  label={`I have read and agree to the <a href="/terms-and-conditions/booking" target="blank">Terms and Conditions</a>`}
                  required="true"
                  type="checkbox"
                  error={error.confirmPassword}
                  name="termsAndCond"
                  id="termsAndCond"
                  value="false"
                  handleChange={handleChange}
                />

                {!processing ? (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    class="theme_button color1"
                  >
                    Start Now
                  </button>
                ) : (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    disabled
                  >
                    Processing...
                  </button>
                )}
                {error && error.submit ? (
                  <span
                    className="alert alert-danger d-block"
                    style={{
                      padding: "1px 20px",
                      marginTop: "10px",
                      display: "block",
                    }}
                  >
                    {error.submit}
                  </span>
                ) : (
                  ""
                )}
              </form>
              <div className={completed === false ? "d-none" : ""}>
                Your subscription has been completed! Please check your email
                and confirm your subscription.
              </div>
              <div
                class="col-lg-12 text-lg-center  scaleAppear"
                style={{ textAlign: "center" }}
              >
                <hr class="divider-search-bar" />
              </div>
              Already registered?{" "}
              <span
                onClick={() => redirectTo(adminSite)}
                to={adminSite}
                className={`color-green c-underline-over`}
              >
                Click here
              </span>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default AdvertiseSignUp;
