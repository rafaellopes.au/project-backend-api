import React, { useEffect } from "react";
import PlataformDetails from "./plataformDetails";
import { Link } from "react-router-dom";
import BreadCrumbs from "../common/breadCrumbs";
import DocumentMeta from "react-document-meta";
import { meta_title_suffix } from "../../config.json";

const Advertise = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const meta = {
    title: `Place an Ad${meta_title_suffix}`,
  };
  return (
    <>
      <DocumentMeta {...meta} />
      <BreadCrumbs data={[{ src: "#", label: "Advertise" }]} />
      <section className="ds section_padding_70 columns_padding_5">
        <div className="container">
          <div className="row bottommargin_20">
            <div className="col-12 text-center">
              <h1>Advertise</h1>
            </div>

            <div className="col-12 text-center">
              <h2 className="topmargin_0">Reclaim your time</h2>
              <p className="entry-excerpt bottommargin_30">
                with integrated online booking + payment software
              </p>
              <h2 className="topmargin_0">Enhance your wellbeing</h2>
              <p className="entry-excerpt bottommargin_30">
                with our wellness membership platform
              </p>
              <h2 className="topmargin_0">
                Be taken seriously as an entrepreneur{" "}
              </h2>
              <p className="entry-excerpt bottommargin_30">
                and a leader in the evolution of the sex industry
              </p>
              <hr style={{ borderBottom: "1px solid white", width: "50%" }} />
              <p>
                Created by a former professional escort, for today’s
                professional escorts, Bellainti is the first platform pioneering
                modern business technology to transform the lives of sex workers
                whilst evolving the entire industry.
              </p>
              <p>
                The ultimate self ownership experience, Bellainti advertisers
                elevate their business beyond all limitations, accessing the
                software and systems that reclaim their time and put them on a
                whole new level as an entrepreneur.
              </p>
              <hr style={{ borderBottom: "1px solid white", width: "50%" }} />
              <p>
                After experiencing 25 years of discrimination and frustration as
                a sex worker, Bellainti was created to empower you as an
                entrepreneur and to elevate the industry in limitless bounds.
                Giving you not just the responsibilities of independent
                entrepreneurship, but the rights you’re entitled to as well.{" "}
              </p>

              <p>
                The sex industry is evolving and anything less will soon fall
                away - will you be part of the change?
              </p>

              <PlataformDetails />
            </div>
          </div>
          <div className="row">
            <div class="col-sm-12  text-center">
              <h3 class="bottommargin_50 big text-center">Packages</h3>
            </div>
          </div>
          <div className="row display_table_md">
            <div className="col-md-4 display_table_cell_md">
              <div className="price-table with_background">
                <div className="plan-name text-uppercase darkgrey_bg_color">
                  <h3>Emerge </h3>
                </div>
                <div className="plan-price cs">
                  <span>$</span>
                  <span>120</span>
                  <p>per session</p>
                </div>
                <div className="features-list">
                  <ul>
                    <li>
                      Your classic entry-level experience to elevate your
                      exposure in an empowered arena.
                    </li>
                    <li>
                      For the curious, the casual or the aspiring escort with a
                      desire to dip their toes into the more evolved end of the
                      industry and bloom from well-built beginnings.
                    </li>
                    <li>
                      Ideal for professionals who work intermittently from one
                      location, escort alongside another career or are content
                      with the current manual systems that manage their
                      business.
                    </li>
                  </ul>
                </div>
                <div className="call-to-action">
                  <Link
                    to={"/advertise/signup"}
                    className="theme_button inverse"
                  >
                    sign up
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-md-4 display_table_cell_md">
              <div className="price-table with_background">
                <div className="plan-name text-uppercase darkgrey_bg_color">
                  <h3>Ecstasy</h3>
                </div>
                <div className="plan-price cs">
                  <span>$</span>
                  <span>499</span>
                  <p>per mounth</p>
                </div>
                <div className="features-list">
                  <ul>
                    <li>
                      Taking your engagement to a new digital edge & evolving
                      life as you know it.
                    </li>
                    <li>
                      For the experienced escort who means business and dares to
                      be boldly independent, well beyond the basics.
                    </li>
                    <li>
                      Ideal for entrepreneurial escorts who want the pleasure of
                      turning on their next tour whilst turning off their phone.
                      The ultimate level up to leadership, as technology tracks
                      your business gains whilst you gain your time back.
                    </li>
                  </ul>
                </div>
                <div className="call-to-action">
                  <Link
                    to={"/advertise/signup"}
                    className="theme_button inverse"
                  >
                    sign up
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-md-4 display_table_cell_md">
              <div className="price-table with_background">
                <div className="plan-name text-uppercase darkgrey_bg_color">
                  <h3>Extended</h3>
                </div>
                <div className="plan-price cs">
                  <span>$</span>
                  <span>999</span>
                  <p>per year</p>
                </div>
                <div className="features-list">
                  <ul>
                    <li>
                      Your premier portal into the profession of the future.
                    </li>
                    <li>
                      For the career sex worker pioneering a business that feels
                      like pleasure from end to end.
                    </li>
                    <li>
                      Ideal for powerhouse professionals who are potently
                      leading the way whilst prioritising prosperity and play.
                      An appreciating asset to your summoning of success,
                      Ecstasy is and always has been your birthright. Sky's the
                      limit.
                    </li>
                  </ul>
                </div>
                <div className="call-to-action">
                  <Link
                    to={"/advertise/signup"}
                    className="theme_button inverse"
                  >
                    sign up
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Advertise;
