import React from "react";

import PersonalDetailItem from "./personalDetailItem";
import CtaBtn from "./ctaBtn";
import CtaBooking from "./ctaBooking";
import { imageFolder } from "../../config.json";
import FavouriteBtn from "./favouriteBtn";

const PersonalDetails = (props) => {
  return (
    <div className="row c-bg-black">
      <div className="col-md-5 c-p-0">
        {props.data &&
        props.user_id &&
        props.avatar &&
        props.avatar.thumbnail ? (
          <picture>
            <source
              srcset={`${imageFolder}/${props.user_id}/${props.avatar.thumbnail}`}
              alt={props.nickname}
              type="image/webp"
            />
            <source
              srcset={`${imageFolder}/${props.user_id}/${props.avatar["thumbnail-web"]}`}
              type="image/jpeg"
              alt={props.nickname}
            />
            <img
              src={`${imageFolder}/${props.user_id}/${props.avatar["thumbnail-web"]}`}
              alt={props.nickname}
            />
          </picture>
        ) : (
          <img
            src="/assets/images/profile_placeholder.png"
            alt=""
            style={{ width: "100%" }}
          />
        )}
      </div>
      <div className="col-md-7 with_padding">
        <h1 className="big topmargin_25 bottommargin_0">
          {props.data && props.nickname ? props.nickname : ""}{" "}
          <FavouriteBtn
            favourite={props.favourite}
            handleFav={props.handleFav}
          />
        </h1>
        <h2 className="topmargin_0 bottommargin_25 color-green font-weight-200">
          {props.data.age} | {props.data.contact.city}{" "}
          {props.data.contact.state}
        </h2>
        {props.data.in_call_rates === 1 || props.data.out_call_rates === 1 ? (
          <>
            {" "}
            <p className="topmargin_50" style={{ fontSize: 25 }}>
              Places of Service
            </p>
            <p
              className="bottommargin_25 color-green font-weight-200"
              style={{ fontSize: 25 }}
            >
              {props.data.in_call_rates === 1 ? "Incall" : ""}
              {props.data.in_call_rates === 1 && props.data.out_call_rates === 1
                ? ", "
                : ""}
              {props.data.out_call_rates === 1 ? "Outcall" : ""}
            </p>
          </>
        ) : (
          ""
        )}

        {props.data.contact ? (
          <CtaBtn
            data={props.data.contact}
            nickname={props.nickname}
            user_id={props.user_id}
          />
        ) : (
          ""
        )}
        <CtaBooking
          nickname={props.nickname}
          data={props.availability}
          user_id={props.user_id}
          avatar={props.avatar}
        />

        <div
          className="row bg-green topmargin_25"
          style={{ color: "white", padding: 10 }}
        >
          <PersonalDetailItem
            label="Height"
            value={props.data.height}
            unit="cm"
          />
          <PersonalDetailItem label="Body Type" value={props.data.body_type} />
          <PersonalDetailItem label="Eyes" value={props.data.eyes} />
          <PersonalDetailItem label="Bust" value={props.data.bust} />
          <PersonalDetailItem
            label="Dress Size"
            value={props.data.dress_size}
          />
          <PersonalDetailItem
            label="Hair Color"
            value={props.data.hair_color}
          />
        </div>
      </div>
    </div>
  );
};

export default PersonalDetails;
