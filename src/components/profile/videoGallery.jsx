import React from "react";
import { imageFolder } from "../../config.json";

const VideoGallery = (props) => {
  return (
    <>
      <h2>VideoGallery</h2>
      <div className="" style={{ marginTop: 50 }}>
        {props.data.map((item) => (
          <div className="col-md-4">
            <video controls width="100%">
              <source
                src={`${imageFolder}/${props.user_id}/videos/${item.file}`}
                type="video/mp4"
              />
              Your browser does not support the video tag.
            </video>
          </div>
        ))}
      </div>
    </>
  );
};

export default VideoGallery;
