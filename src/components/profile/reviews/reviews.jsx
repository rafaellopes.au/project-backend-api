import React, { useState } from "react";
import { Link } from "react-router-dom";
import { getCurrentMember } from "../../../services/memberServices";
import _ from "lodash";

const Reviews = ({ data, handleAddReview, submitedReview }) => {
  const [comment, setComment] = useState("");
  const [prevRate, setPrevRate] = useState();
  const [rate, setRate] = useState(0);
  const [rateErr, setRateErr] = useState("");
  const [commentErr, setCommentErr] = useState("");

  const showState = (num) => {
    let stars = [];
    for (let i = 0; i < 5; i++) {
      if (i < num) {
        stars.push(<i className="author_url fa fa-star color-green"></i>);
      } else {
        stars.push(<i className="author_url fa fa-star-o color-green"></i>);
      }
    }
    return <>{stars}</>;
  };

  const sendAReview = () => {
    if (rate === 0) {
      return setRateErr("Please, select you rate between 1 - 5 start.");
    }
    if (comment.length < 15) {
      return setCommentErr("Your comment must have at least 15 characteres.");
    }
    handleAddReview(rate, comment);
  };

  const getApprovedReviews = () => {
    if (data && data.length > 0) {
      const countApproved = _.filter(data, function (o) {
        return o.status === "approved";
      });

      let avgScore = _.meanBy(countApproved, function (o) {
        return o.rate;
      });

      const element = [];
      for (let i = 0; i < 5; i++) {
        element.push(
          <i
            class={`fa fa-star${
              avgScore.toFixed(0) > i ? "" : "-o"
            } color-green`}
          ></i>
        );
      }

      return (
        <>
          <h2>
            {avgScore.toFixed(1)} {element} - {countApproved.length} Review{" "}
            {countApproved.length > 1 ? "s" : ""}
          </h2>
          <hr style={{ borderTop: "1px solid" }} />
        </>
      );
    } else {
      return <p>No reviews received yet.</p>;
    }
  };

  return (
    <div>
      <div class="comment-respond" id="respond">
        {data.length === 0 ? (
          <>
            <h2>Reviews</h2>
            {submitedReview !== 1 ? <p>No reviews received yet.</p> : ""}
          </>
        ) : (
          <>{getApprovedReviews()}</>
        )}
        {localStorage.getItem("tokenKey") ? (
          <>
            {submitedReview === 1 ? (
              <>
                Thank you! Your review has been submited and will be validate
                soon.
              </>
            ) : (
              <>
                <div class="row columns_padding_5">
                  <div class="col-md-12">
                    <p>
                      Display name: <b>{getCurrentMember().name}</b>
                    </p>
                    <h3 style={{ marginBottom: "0px" }}>
                      {[1, 2, 3, 4, 5].map((item) => (
                        <>
                          <i
                            class={`fa 
                            
                              ${
                                prevRate
                                  ? prevRate >= item
                                    ? "fa-star"
                                    : "fa-star-o"
                                  : rate >= item
                                  ? "fa-star"
                                  : "fa-star-o"
                              }

                             color-green`}
                            onMouseOver={() => setPrevRate(item)}
                            onMouseOut={() => setPrevRate()}
                            onClick={() => {
                              setRate(item);
                              setRateErr();
                            }}
                          ></i>{" "}
                        </>
                      ))}
                    </h3>
                    <p className="text-danger">{rateErr}</p>
                    <p class="comment-form-chat">
                      <label for="comment" class="sr-only">
                        Comment
                      </label>
                      <textarea
                        aria-required="true"
                        rows="8"
                        cols="45"
                        name="comment"
                        id="comment"
                        class="form-control with-icon"
                        placeholder=""
                        onChange={(e) => {
                          setComment(e.target.value);
                          setCommentErr("");
                        }}
                      >
                        {comment}
                      </textarea>
                      <p className="text-danger">{commentErr}</p>
                    </p>
                  </div>
                </div>
                <p class="form-submit">
                  <button
                    type="button"
                    id="send"
                    name="send"
                    class="theme_button wide_button color1"
                    onClick={sendAReview}
                  >
                    Send
                  </button>
                  {submitedReview === -1 ? (
                    <p className="text-danger">
                      Error, your comment coldn't be submited!
                    </p>
                  ) : (
                    ""
                  )}
                </p>
              </>
            )}
          </>
        ) : (
          <p style={{ marginTop: "30px" }}>
            Only logged in members may leave a review.
            <br />
            Click <Link to="/member">here</Link> to log in or{" "}
            <Link to="/member">subscribe now</Link>.
          </p>
        )}
      </div>

      <div class="comments-area" id="comments">
        <ol class="comment-list">
          {data.length > 0 &&
            data.map((item) => (
              <li class="comment even thread-even depth-1 parent">
                <article class="comment-body media">
                  <div
                    className={
                      item.status === "pending"
                        ? "bgAPending media-body ls"
                        : "media-body ls"
                    }
                  >
                    <span class="reply">
                      <a class="theme_button color1" href="#respond">
                        <i class="rt-icon2-arrow-back-outline"></i>
                      </a>
                    </span>

                    <div class="comment-meta darklinks">
                      <h3 class="author_url">
                        {item.member_name}{" "}
                        {item.status === "pending" ? (
                          <span style={{ fontSize: "14px" }}>
                            {" "}
                            (Pending Approval)
                          </span>
                        ) : (
                          ""
                        )}
                      </h3>
                      <span
                        class="comment-date d-block"
                        style={{ display: "block" }}
                      >
                        <time
                          datetime="2015-11-08T15:05:23+00:00"
                          class="entry-date"
                        >
                          {item.date && item.date.substring(6, 8)}/
                          {item.date && item.date.substring(4, 6)}/
                          {item.date && item.date.substring(0, 4)}{" "}
                          {item.date && item.date.substring(8, 10)}:
                          {item.date && item.date.substring(10, 12)}
                        </time>
                      </span>

                      <br />
                      <span style={{ marginTop: "10px", display: "block" }}>
                        {showState(item.rate)}
                      </span>
                    </div>
                    <p>{item.review}</p>
                  </div>
                </article>
              </li>
            ))}
        </ol>
      </div>
    </div>
  );
};

export default Reviews;
