import React from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import gaFire from "../common/gaFire";

const CtaBtn = (props) => {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
    gaFire.showMobileNumber(props.user_id);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <button
        className={`btn btn-success cta-profile-bt pulse ${
          props.classname || ""
        }`}
        onClick={handleClickOpen}
      >
        Call/Text me now!
      </button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Close
          </Button>
        </DialogActions>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <div>
              <h4 style={{ fontSize: "28px" }}>
                Contact <b>{props.nickname}</b>
              </h4>
              {props.data.phone
                ? props.data.phone.map((item) => (
                    <p
                      style={{
                        fontSize: "25px",
                        marginTop: "25px",
                      }}
                    >
                      <Link to={`tel:${item.phone_number}`}>
                        {item.phone_number}{" "}
                        <i className="rt-icon2-mobile-phone"></i>
                      </Link>{" "}
                      {item.call === true && item.sms === false ? (
                        <span className="badge alert-danger">Call only</span>
                      ) : (
                        ""
                      )}
                      {item.call === false && item.sms === true ? (
                        <span className="badge alert-danger">Text only</span>
                      ) : (
                        ""
                      )}
                    </p>
                  ))
                : ""}
              {props.data.noCallerId && props.data.noCallerId === true ? (
                <span className="badge alert-danger">
                  Does not accept calls from 'No Caller Id'
                </span>
              ) : (
                ""
              )}
              {props.data.instructions ? (
                <>
                  <h4 style={{ fontSize: "28px" }}>Preferred Contact Method</h4>

                  {props.data && props.data.instructions
                    ? props.data.instructions
                        .split(/(\r\n|\n|\r)/gm)
                        .map((value) =>
                          value.toString().length > 0 ? (
                            <p
                              style={{
                                fontSize: "17px",
                                marginBottom: "10px",
                              }}
                            >
                              {value}
                            </p>
                          ) : (
                            ""
                          )
                        )
                    : ""}
                </>
              ) : (
                ""
              )}
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default CtaBtn;
