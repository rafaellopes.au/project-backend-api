import React, { useState } from "react";
import Joi from "joi-browser";
import { RenderInput, validateProperty, RenderSelect } from "../../common/form";
import { verify, confirmBooking } from "../../../services/bookingServices";
import format from "date-format";

const BookingConfirm = (props) => {
  const [formData, setFormData] = useState({
    name: "",
    mobile: "",
    confirm: "",
    typeservice: "Incall",
    suburb: "",
  });
  const schema = {
    name: Joi.string().min(2).max(15).required().label("Name"),
    mobile: Joi.string().min(10).max(10).required().label("Phone Number"),
    confirm: Joi.string().max(6).label("Confirmation code"),
    typeservice: Joi.string().required().label("Gender"),
    suburb: Joi.string().allow("").label("suburb"),
  };
  const [errorFormMsg, setErrorFormMsg] = useState();
  const [validationSent, setValidationSent] = useState(false);
  const [error, setError] = useState({});

  const handleChange = (k, v) => {
    let newData = { ...formData };
    newData[k] = v;
    setFormData(newData);
    const validate = validateProperty(k, v, schema);

    let newError = { ...error };
    newError[k] = validate;
    setError(newError);
  };

  const sendValidation = async () => {
    try {
      if (!formData.mobile) {
        setErrorFormMsg("Invalid mobile number!");
        setTimeout(() => {
          setErrorFormMsg();
        }, 6000);
      }
      await verify(formData.mobile);
      setValidationSent(true);
    } catch (error) {
      setErrorFormMsg(
        "Internal erro, pelase try again or contact out support team!"
      );
      setTimeout(() => {
        setErrorFormMsg();
      }, 6000);
    }
  };

  const handleNext = () => {
    if (
      !formData.name ||
      !formData.mobile ||
      (formData.typeservice === "Outcall" && formData.suburb === "")
    ) {
      setErrorFormMsg("Please fill in all required fields!");
      setTimeout(() => {
        setErrorFormMsg();
      }, 6000);
      return;
    }
    if (formData.mobile.length !== 10) {
      setErrorFormMsg("Invalid mobile number format!");
      setTimeout(() => {
        setErrorFormMsg();
      }, 6000);
      return;
    }
    sendValidation();
  };

  const handleConfirm = async () => {
    try {
      const { name, mobile, confirm, typeservice, suburb } = formData;
      const { period, starts } = props.selectedPeriod;
      await confirmBooking({
        name,
        mobile,
        typeservice,
        suburb,
        code: confirm,
        day: format("yyyy-MM-dd", props.selectedDay),
        period,
        starts,
        user_id: props.user_id,
      });
      props.bookingConfirmed({
        name,
        mobile: "******" + mobile.substring(6, 10),
        typeservice,
        suburb,
        day: format("dd/MM/yyyy", props.selectedDay),
        period,
        starts,
      });
    } catch (error) {
      if (error.response && error.response.status === 400) {
        setErrorFormMsg("Invalid or Expired confirmation code!");
      } else {
        setErrorFormMsg("Internal Error!");
      }
      setTimeout(() => {
        setErrorFormMsg();
      }, 6000);
    }
  };

  const handleChangeNumber = () => {
    setValidationSent();
    handleChange("confirm", "");
  };

  return (
    <div className="container c-mobile-p-top-0">
      <div className="row">
        <div className="col">
          <div className="row">
            {props.selectedDay && (
              <>
                <h3>Confirm your booking</h3>
                <h4 className="confirm-h4">
                  {" "}
                  Selected day:{" "}
                  <strong>{props.selectedDay.toLocaleDateString()}</strong>{" "}
                  <span
                    style={{
                      fontSize: "10px",
                      fontWeight: "bold",
                      color: "Green",
                    }}
                    onClick={props.handleChangeDay}
                    className="c-underline-over"
                  >
                    (Change)
                  </span>
                </h4>
                <h4 className="confirm-h4">
                  Selected period:{" "}
                  <strong>
                    {props.selectedPeriod.starts / props.slotSize + ":00"} -{" "}
                    {(props.selectedPeriod.starts +
                      props.selectedPeriod.period) /
                      props.slotSize +
                      ":00"}
                  </strong>{" "}
                  <span
                    style={{
                      fontSize: "10px",
                      fontWeight: "bold",
                      color: "Green",
                    }}
                    onClick={props.handleChangePeriod}
                    className="c-underline-over"
                  >
                    (Change)
                  </span>
                </h4>
                {!validationSent ? (
                  <form class="contact-form" method="post" action="/">
                    <RenderSelect
                      label="Type"
                      required="true"
                      options={[
                        { k: "Incall", v: "Incall" },
                        { k: "Outcall", v: "Outcall" },
                      ]}
                      value={formData.typeservice}
                      name="typeservice"
                      id="typeservice"
                      handleChange={handleChange}
                    />

                    {formData.typeservice &&
                    formData.typeservice === "Outcall" ? (
                      <RenderInput
                        label="Suburb"
                        required="false"
                        value={formData.suburb}
                        error={error.suburb}
                        name="suburb"
                        id="suburb"
                        handleChange={handleChange}
                      />
                    ) : (
                      ""
                    )}

                    <RenderInput
                      label="Name"
                      required="true"
                      value={formData.name}
                      error={error.name}
                      name="name"
                      id="name"
                      handleChange={handleChange}
                    />

                    <RenderInput
                      label="Mobile Number"
                      required="true"
                      value={formData.mobile}
                      error={error.mobile}
                      name="mobile"
                      id="mobile"
                      handleChange={handleChange}
                    />

                    <span
                      class={`btn cta-profile-bt btn-booking-success`}
                      onClick={handleNext}
                      style={{ padding: "15px" }}
                    >
                      Next
                    </span>
                    {!errorFormMsg ? (
                      ""
                    ) : (
                      <span
                        class="alert alert-danger"
                        style={{ padding: "1px 20px", marginTop: "10px" }}
                      >
                        {errorFormMsg}
                      </span>
                    )}
                  </form>
                ) : (
                  <form class="contact-form" method="post" action="/">
                    <h4 className="confirm-h4">
                      {" "}
                      Name: <strong>{formData.name}</strong>
                    </h4>
                    <h4 className="confirm-h4">
                      {" "}
                      Mobile: <strong>{formData.mobile}</strong>{" "}
                      <span
                        style={{
                          fontSize: "10px",
                          fontWeight: "bold",
                          color: "Green",
                        }}
                        onClick={handleChangeNumber}
                        className="c-underline-over"
                      >
                        (Change number)
                      </span>
                    </h4>
                    <h4 className="confirm-h4">
                      {" "}
                      Type: <strong>{formData.typeservice}</strong>
                    </h4>
                    {formData.suburb ? (
                      <h4 className="confirm-h4">
                        {" "}
                        Suburb: <strong>{formData.suburb}</strong>
                      </h4>
                    ) : (
                      ""
                    )}
                    <h5
                      style={{
                        fontWeight: "bold",
                        marginTop: "5px",
                      }}
                    >
                      Please confirm the code of confirmation sent to your
                      mobile number:
                    </h5>
                    <RenderInput
                      label="Confirmation number"
                      required="true"
                      value={formData.confirm}
                      error={error.confirm}
                      name="confirm"
                      id="confirm"
                      handleChange={handleChange}
                    />
                    <span
                      class={`btn cta-profile-bt btn-booking-success`}
                      onClick={handleConfirm}
                      style={{ padding: "15px" }}
                    >
                      Confirm
                    </span>
                    {errorFormMsg && (
                      <span
                        class="alert alert-danger"
                        style={{
                          padding: "1px 20px",
                          marginTop: "10px",
                          fontSize: "15px",
                        }}
                      >
                        {errorFormMsg}
                      </span>
                    )}
                  </form>
                )}

                {}
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default BookingConfirm;
