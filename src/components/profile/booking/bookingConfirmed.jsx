import React from "react";
import gaFire from "../../common/gaFire";

const BookingConfirmed = (props) => {
  //const { mobile, starts, period } = props.data;
  const {
    starts,
    period,
    name,
    mobile,
    day,
    suburb,
    typeservice,
    user_id,
  } = props.data;
  gaFire.bookingRequest(user_id);
  return (
    <div className="container c-mobile-p-top-0">
      <div className="row">
        <div className="col">
          <div className="row" style={{ marginTop: "10px" }}>
            <h4>
              We just sent your message to <strong>{props.nickname}</strong>.
              Integer sit amet vulputate metus, vitae finibus lacus.
            </h4>
          </div>
          <div className="row">
            <div>
              <h5>Your details:</h5>
              <p className="p-booking">
                Name: <strong>{name}</strong>
              </p>
              <p className="p-booking">
                Mobile Number: <strong>{mobile}</strong>
              </p>
              <p className="p-booking">
                Type: <strong>{typeservice}</strong>
              </p>
              {suburb ? (
                <p className="p-booking">
                  Suburb: <strong>{suburb}</strong>
                </p>
              ) : (
                ""
              )}
              <p className="p-booking">
                Date: <strong>{day}</strong>
              </p>
              <p className="p-booking">
                Period:{" "}
                <strong>
                  {starts / 60}:00 {(period + starts) / 60}:00
                </strong>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BookingConfirmed;
