import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import _ from "lodash";
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";
import BookingConfirm from "./booking/bookingConfirm";
import BookingConfirmed from "./booking/bookingConfirmed";
import bookingServices from "../../services/bookingServices";
import format from "date-format";
import { imageFolder } from "../../config.json";

const CtaBooking = (props) => {
  const [open, setOpen] = useState(false);
  const [bookingStep, setBookingStep] = useState(1);
  const [selectedDay, setSelectedDay] = useState();
  const [wrongSelection, setWrongSelection] = useState("");
  const [selectedPeriod, setSelectedPeriod] = useState();
  const [dayOfWeekAvailability, setDayOfWeekAvailability] = useState({
    available: [],
    notavailable: [0, 1, 2, 3, 4, 5, 6],
  });
  const [slots, setSlots] = useState([]);
  const [showSlots, setShowSlots] = useState();
  const [bookedData, setBookedData] = useState();

  const slotSize = 60;

  const modifiersStyles = {
    availableDays: {
      color: "#ffc107",
    },
    notAvailableDays: {
      color: "rgb(212 212 212 / 54%)",
    },
    today: {
      color: "#4A90E2",
    },
  };

  const availability = () => {
    let objAvailability = { available: [], notavailable: [] };
    for (let i = 0; i < props.data.length; i++) {
      if (props.data[i].available) {
        objAvailability.available.push(i);
      } else {
        objAvailability.notavailable.push(i);
      }
    }
    setDayOfWeekAvailability(objAvailability);
  };

  const modifiers = {
    availableDays: { daysOfWeek: dayOfWeekAvailability.available },
    notAvailableDays: { daysOfWeek: dayOfWeekAvailability.notavailable },
  };

  const handleClickOpen = () => {
    setSelectedDay();
    setSelectedPeriod();
    setBookingStep(1);
    setWrongSelection("");
    availability();
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDayClick = (day) => {
    setSelectedPeriod();
    const isValid = _.findIndex(dayOfWeekAvailability.available, function (o) {
      return o === day.getDay();
    });
    console.log("isValid", isValid);
    console.log(
      "dts",
      day.setHours(0, 0, 0, 0),
      new Date().setHours(0, 0, 0, 0)
    );
    if (
      isValid !== -1 &&
      day.setHours(0, 0, 0, 0) >= new Date().setHours(0, 0, 0, 0)
    ) {
      console.log("jere");
      setSelectedDay(day);
    } else {
      console.log("err");
      setSelectedDay();
      setWrongSelection("Invalid selection");
    }
  };

  const bookingConfirmed = (details) => {
    setSelectedDay();
    setWrongSelection("");
    setSelectedPeriod();
    setSlots([]);
    setBookedData(details);
    setBookingStep(3);
  };

  const handleChangeDay = () => {
    setSelectedPeriod();
    setSelectedDay();
    setBookingStep(1);
  };

  const handleChangePeriod = () => {
    setSelectedPeriod();
    setBookingStep(1);
  };

  useEffect(() => {
    setSelectedDay();
    setSelectedPeriod();
    availability();
  }, []);

  const populateSlots = (from, to, selectedDay) => {
    //slots
    let arrSlots = [];
    //convert to minutes
    const minutesFrom = from.split(":")[0] * 60;
    const minutesTo = to.split(":")[0] * 60;
    //slotSize
    if (minutesFrom >= minutesTo) {
      return arrSlots;
    }

    let slotStart = minutesFrom;
    while (slotStart < minutesTo) {
      let isBusy = false;
      if (
        new Date(
          new Date(selectedDay).toDateString() +
            " " +
            (slotStart - 60) / slotSize +
            ":00"
        ) <= new Date()
      ) {
        isBusy = true;
      }
      arrSlots.push({
        minute: slotStart,
        time: slotStart / slotSize + ":00",
        isBusy,
      });
      slotStart = slotStart + slotSize;
    }
    return arrSlots;
  };

  const checkIfAvailable = async (arrSlots, date) => {
    try {
      const appointments = await bookingServices.agenda(
        props.user_id,
        format("yyyy-MM-dd", selectedDay)
      );
      if (appointments.data) {
        console.log(appointments.data);
        let slotIndex = -1;

        for (let i = 0; i < appointments.data.length; i++) {
          slotIndex = _.findIndex(arrSlots, function (o) {
            return o.minute === appointments.data[i].starts;
          });
          if (slotIndex !== -1) {
            for (let j = 0; j < appointments.data[i].period / slotSize; j++) {
              arrSlots[slotIndex + j].isBusy = true;
            }
          }
        }
      }

      return arrSlots;
    } catch (error) {
      return [];
    }
  };

  useEffect(() => {
    const doThis = async () => {
      if (!selectedDay) {
        return setSlots([]);
      }

      const from = props.data[selectedDay.getDay()].from;
      const to = props.data[selectedDay.getDay()].to;

      let arrSlots = populateSlots(from, to, selectedDay);
      arrSlots = await checkIfAvailable(arrSlots, selectedDay);

      setSlots(arrSlots);
    };
    doThis();
  }, [selectedDay]);

  useEffect(() => {
    mountVisualSlots();
  }, [slots]);

  const handleSelect = (selectedSlot, index) => {
    if (selectedSlot.isBusy) {
      return null;
    }
    let updateSlots = slots;
    let newstatus = false;
    if (updateSlots[index].isSelected) {
      updateSlots[index].isSelected = false;
    } else {
      updateSlots[index].isSelected = true;
      newstatus = true;
    }

    const firstSelected = _.findIndex(updateSlots, ["isSelected", true]);
    const lastSelected = _.findLastIndex(updateSlots, ["isSelected", true]);

    if (firstSelected !== lastSelected) {
      let failed = 0;
      for (let i = firstSelected; i < lastSelected; i++) {
        if (!updateSlots[i].isSelected) {
          failed++;
        }
      }
      if (failed > 0) {
        for (let i = firstSelected; i <= lastSelected; i++) {
          if (i !== index) {
            updateSlots[i].isSelected = false;
          }
        }
      }
    }
    setSlots(updateSlots);
    if (firstSelected > -1) {
      const period = (lastSelected - firstSelected) * slotSize + slotSize;
      setSelectedPeriod({
        starts: updateSlots[firstSelected].minute,
        period,
      });
    } else {
      setSelectedPeriod();
    }

    mountVisualSlots();
  };

  const mountVisualSlots = () => {
    const creatVisualSlots = slots.map((slot, index) => (
      <span
        class={`badge ${slot.isBusy ? "color-off" : "c-underline-over"} 
          ${!slot.isBusy && slot.isSelected ? "color-select" : ""} 
          ${
            !slot.isBusy && !slot.isSelected ? "color-available" : ""
          } c-w-100 c-mb-5`}
        onClick={() => handleSelect(slot, index)}
        style={{ fontSize: "14px" }}
      >
        {slot.time}
      </span>
    ));
    setShowSlots(creatVisualSlots);
  };

  return (
    <>
      <button
        className={`btn btn-succeinfoss cta-profile-bt ${
          props.classname || ""
        }`}
        onClick={handleClickOpen}
      >
        <i className="rt-icon2-calendar"></i> Request a Booking
      </button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Close
          </Button>
        </DialogActions>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {localStorage.getItem("tokenKey") ? (
              <div className="container c-mobile-p-top-0">
                <div className="row">
                  <div className="col">
                    {props.avatar && props.avatar.thumbnail ? (
                      <picture>
                        <source
                          srcset={`${imageFolder}/${props.user_id}/${props.avatar.thumbnail}`}
                          alt={props.nickname}
                          type="image/webp"
                        />
                        <source
                          srcset={`${imageFolder}/${props.user_id}/${props.avatar["thumbnail-web"]}`}
                          type="image/jpeg"
                          alt={props.nickname}
                        />
                        <img
                          src={`${imageFolder}/${props.user_id}/${props.avatar["thumbnail-web"]}`}
                          alt={props.nickname}
                          width="80"
                          style={{
                            borderRadius: "100%",
                            marginRight: "20px",
                            border: "3px solid #3c763d",
                          }}
                          align="left"
                        />
                      </picture>
                    ) : (
                      <img
                        src="/assets/images/profile_placeholder.png"
                        alt=""
                        width="80"
                        style={{
                          borderRadius: "100%",
                          marginRight: "20px",
                          border: "3px solid #3c763d",
                        }}
                        align="left"
                      />
                    )}

                    <h4 style={{ fontSize: "28px" }}>
                      Request a Booking
                      <br />
                      <b>{props.nickname}</b>
                    </h4>
                    <span className="badge alert-danger">
                      Donec lobortis quis nisi sed aliquet.
                    </span>
                  </div>
                </div>
                {bookingStep === 1 && (
                  <div className="row">
                    <div className="col-sm-6">
                      <DayPicker
                        disabledDays={dayOfWeekAvailability.notavailable}
                        onDayClick={handleDayClick}
                        selectedDays={selectedDay}
                        modifiers={modifiers}
                        modifiersStyles={modifiersStyles}
                      />
                    </div>
                    <div className="col-sm-6">
                      {selectedDay ? (
                        <>
                          <h4>Your selection</h4>
                          <h5>
                            Selected day:{" "}
                            <strong>{selectedDay.toLocaleDateString()}</strong>
                          </h5>
                          {!selectedPeriod ? (
                            ""
                          ) : (
                            <>
                              <h5>
                                Selected period:{" "}
                                <strong>
                                  {selectedPeriod.starts / slotSize + ":00"} -{" "}
                                  {(selectedPeriod.starts +
                                    selectedPeriod.period) /
                                    slotSize +
                                    ":00"}
                                </strong>
                              </h5>
                              {selectedPeriod && (
                                <button
                                  class="btn cta-profile-bt btn-booking-success"
                                  onClick={() => {
                                    setBookingStep(2);
                                  }}
                                  style={{ padding: "15px" }}
                                >
                                  Next
                                </button>
                              )}
                            </>
                          )}

                          <div className="container c-mobile-p-top-0">
                            <div className="row">
                              <div className="col">{showSlots}</div>
                            </div>
                          </div>
                        </>
                      ) : (
                        <>
                          <h4>Pick a date</h4>
                          {wrongSelection ? <h5>{wrongSelection}</h5> : ""}
                        </>
                      )}
                    </div>
                  </div>
                )}
                {bookingStep === 2 && (
                  <BookingConfirm
                    selectedDay={selectedDay}
                    selectedPeriod={selectedPeriod}
                    slotSize={slotSize}
                    user_id={props.user_id}
                    bookingConfirmed={bookingConfirmed}
                    handleChangeDay={handleChangeDay}
                    handleChangePeriod={handleChangePeriod}
                  />
                )}
                {bookingStep === 3 && (
                  <BookingConfirmed
                    data={bookedData}
                    nickname={props.nickname}
                    user_id={props.user_id}
                  />
                )}
              </div>
            ) : (
              <div className="container c-mobile-p-top-0">
                <div className="row">
                  <div className="col">
                    <h4>
                      Bellaint booking feature is only available for registered
                      members. If you are a member{" "}
                      <Link to="/member">
                        <strong>click here</strong>
                      </Link>{" "}
                      to log in or{" "}
                      <Link to="/member">
                        <strong>subscribe now</strong>
                      </Link>
                      .
                    </h4>
                  </div>
                </div>
              </div>
            )}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default CtaBooking;
