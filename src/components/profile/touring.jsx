import React from "react";

const Touring = (props) => {
  const formatDate = (dt) => {
    let dateFormat = dt.toString();
    return (
      dateFormat.substring(6, 8) +
      "/" +
      dateFormat.substring(4, 6) +
      "/" +
      dateFormat.substring(0, 4)
    );
  };

  return (
    <>
      {props.data.length > 0 ? (
        <div className="row">
          <div className="col-md-12">
            <div>
              <div className="appointment-list c-bg-black">
                <h2 className="color-green big" style={{ marginTop: 0 }}>
                  Touring
                </h2>
                {!props.data || props.data.length === 0 ? (
                  "..."
                ) : (
                  <ul>
                    {props.data.map((item) =>
                      item.from && item.until ? (
                        <li>
                          <div className="book-appointment">
                            <span
                              className="theme_button color1"
                              style={{ fontSize: "11px" }}
                            >
                              {item.city}, {item.state}
                            </span>
                          </div>
                          <div className="appointment-time">
                            <i className="rt-icon2-calendar"></i>
                            <span>
                              {formatDate(item.from)} - {formatDate(item.until)}
                            </span>
                          </div>
                        </li>
                      ) : (
                        ""
                      )
                    )}
                  </ul>
                )}
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
};

export default Touring;
