import React from "react";

const PersonalDetailItem = (props) => {
  return props.value ? (
    <div className={props.col ? props.col : `col-xs-4 col-sm-3 col-md-2`}>
      <span className="bold">{props.label}</span>
      <br />
      <span>
        {props.value}
        {props.unit ? ` ${props.unit}` : ""}
      </span>
    </div>
  ) : (
    ""
  );
};

export default PersonalDetailItem;
