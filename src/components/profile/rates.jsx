import React from "react";

const Rates = (props) => {
  return !props.data || props.data.length === 0 ? (
    ""
  ) : (
    <div class="row">
      <div class="col-sm-12">
        <h2 class="text-center divider_40">{props.label}</h2>
        <table class="table table-striped">
          <tbody>
            {props.data.map((item) => (
              <tr>
                <td className="rates-col-1">{item.service}</td>
                <td className="rates-col-2">{item.price}</td>
                <td className="rates-col-3">{item.additional_details}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Rates;
