import React from "react";
import { Link } from "react-router-dom";

const FavouriteBtn = (props) => {
  return (
    <>
      {props.favourite === -1 ? (
        <Link to="/member">
          <i className="fa fa-heart-o heart-title"></i>
        </Link>
      ) : (
        ""
      )}

      {props.favourite === 0 ? (
        <i className="fa fa-heart-o heart-title" onClick={props.handleFav}></i>
      ) : (
        ""
      )}
      {props.favourite === 1 ? (
        <i className="fa fa-heart heart-title" onClick={props.handleFav}></i>
      ) : (
        ""
      )}
    </>
  );
};

export default FavouriteBtn;
