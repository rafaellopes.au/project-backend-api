import React from "react";

const Availability = (props) => {
  var weekdays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  return (
    <div className="row">
      <div className="col-md-12">
        <div>
          <div className="appointment-list c-bg-black">
            <h2 className="color-green big" style={{ marginTop: 0 }}>
              Availability
            </h2>
            {!props.data || props.data.length === 0 ? (
              "..."
            ) : (
              <ul>
                {props.data.map((item) =>
                  item.from && item.to ? (
                    <li>
                      <div className="book-appointment">
                        <span className="theme_button color1">
                          {weekdays[item.day_of_week]}
                        </span>
                      </div>
                      <div className="appointment-time">
                        <i className="rt-icon2-clock3"></i>
                        <span>
                          {item.from} – {item.to}
                        </span>
                      </div>
                    </li>
                  ) : (
                    ""
                  )
                )}
              </ul>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Availability;
