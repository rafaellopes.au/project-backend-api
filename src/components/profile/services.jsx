import React from "react";

const Services = (props) => {
  return (
    <div class="row fontawesome-icon-list">
      {props.data.map((item) => (
        <div class="col-md-6 col-sm-6">
          <i class="fa fa-angle-double-right"></i> {item}
        </div>
      ))}
    </div>
  );
};

export default Services;
