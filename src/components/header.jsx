import React, { useState } from "react";
import { Link } from "react-router-dom";

function Header(props) {
  const clickLink = (url) => {
    window.location = url;
  };
  const [locations] = useState([
    { name: "Australia", slug: "/australia" },

    {
      name: "ACT",
      slug: "/australia/act",
      nextLevel: [{ name: "Canberra", slug: "/canberra" }],
    },

    {
      name: "NSW",
      slug: "/australia/nsw",
      nextLevel: [
        { name: "Sydney", slug: "/sydney" },
        { name: "Newcastle", slug: "/newcastle" },
        { name: "Wollongong", slug: "/wollongong" },
        { name: "Albury", slug: "/albury" },
      ],
    },

    {
      name: "QLD",
      slug: "/australia/qld",
      nextLevel: [
        { name: "Brisbane", slug: "/brisbane" },
        { name: "Gold Coast", slug: "/gold-Coast" },
        { name: "Sunshine Coast", slug: "/sunshine-coast" },
        { name: "Cairns", slug: "/cairns" },
        { name: "Townsville", slug: "/townsville" },
      ],
    },

    {
      name: "SA",
      slug: "/australia/sa",
      nextLevel: [{ name: "Adelaide", slug: "/adelaide" }],
    },
    {
      name: "Vic",
      slug: "/australia/vic",
      nextLevel: [
        { name: "Melbourne", slug: "/melbourne" },
        { name: "Geelong", slug: "/geelong" },
        { name: "Ballart", slug: "/ballart" },
        { name: "Wodonga", slug: "/wodonga" },
      ],
    },

    {
      name: "WA",
      slug: "/australia/wa",
      nextLevel: [
        { name: "Perth", slug: "/perth" },
        { name: "Mandurah", slug: "/bandurah" },
        { name: "Bunbury", slug: "/bunbury" },
        { name: "Albany", slug: "/albany" },
      ],
    },
  ]);
  return (
    <>
      <header className="page_header header_darkgrey columns_padding_0 table_section">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-3 col-sm-6 text-center">
              <Link to="/" className="logo logo_image">
                <img src="/assets/images/logo.png" alt="" />
              </Link>
            </div>
            <div className="col-md-6 text-center">
              <nav className="mainmenu_wrapper">
                <ul className="mainmenu nav sf-menu">
                  <li>
                    <Link to="#">Locations</Link>
                    <ul>
                      {locations.map((location) => (
                        <li>
                          <Link onClick={() => clickLink(location.slug)}>
                            {location.name}
                          </Link>
                          {location.nextLevel ? (
                            <ul>
                              {location.nextLevel.map((item) => (
                                <li>
                                  {" "}
                                  <Link
                                    onClick={() =>
                                      clickLink(location.slug + item.slug)
                                    }
                                  >
                                    {item.name}
                                  </Link>
                                </li>
                              ))}
                            </ul>
                          ) : (
                            ""
                          )}
                        </li>
                      ))}
                    </ul>
                  </li>
                  <li>
                    <Link onClick={() => clickLink("/escorts")}>Escorts</Link>
                  </li>
                  <li>
                    <Link to="/member">Member</Link>
                  </li>

                  <li>
                    <Link to="/contact">Contact</Link>
                  </li>
                </ul>
              </nav>
              <span className="toggle_menu">
                <span></span>
              </span>
            </div>
            <div className="col-md-3 col-sm-6 header-contacts text-center hidden-xs">
              {""}
            </div>
          </div>
        </div>
      </header>
    </>
  );
}

export default Header;
