import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import SearchComponent from "./search/searchComponent";
import PageContent from "./common/pageContent";

const Locations = (props) => {
  const { city } = useParams();

  const [defaultSearch, setDefaultSearch] = useState();

  useEffect(() => {
    const locSearch = {};
    if (props.match && props.match.params && props.match.params.austate) {
      locSearch.state = [props.match.params.austate.toUpperCase()];
    }
    if (props.match && props.match.params && props.match.params.city) {
      locSearch.city = city.replaceAll(/-/g, " ").toUpperCase();
    }
    if (props.match && props.match.params && props.match.params.suburb) {
      locSearch.suburb = props.match.params.suburb;
    }
    setDefaultSearch(locSearch);

    window.scrollTo(0, 0);
  }, [city, props.match]);

  return (
    <>
      <PageContent slug={props.location.pathname} />

      <section className="ds page_models models_square gorizontal_padding section_padding_70 columns_padding_0">
        {defaultSearch && (
          <SearchComponent
            default={defaultSearch}
            getLocation={false}
            showFilters={true}
            limitPerPage={10}
            {...props}
          />
        )}
      </section>
    </>
  );
};

export default Locations;
