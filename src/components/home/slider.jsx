import React from "react";

const Slider = () => {
  return (
    <section className="ds ms intro_section page_mainslider">
      <div className="flexslider">
        <ul className="slides ds">
          <li>
            <img src="/assets/images/top_slider_01.jpg" alt="" />
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <div className="slide_description_wrapper">
                    <div className="slide_description">
                      <div className="intro-layer" data-animation="expandUp">
                        <h2 className="highlight text-uppercase">Top Models</h2>
                        <h3 className="extra-big text-uppercase">
                          Olivia Torney
                        </h3>
                      </div>
                      <div className="intro-layer" data-animation="expandUp">
                        <div className="model-parameters topmargin_40 bottommargin_40">
                          <div>
                            <span className="bold">Height</span>
                            <br />
                            <span>185</span>
                          </div>
                          <div>
                            <span className="bold">Bust</span>
                            <br />
                            <span>79</span>
                          </div>
                          <div>
                            <span className="bold">Waist</span>
                            <br />
                            <span>59</span>
                          </div>
                          <div>
                            <span className="bold">Hips</span>
                            <br />
                            <span>87</span>
                          </div>
                          <div>
                            <span className="bold">Shoe</span>
                            <br />
                            <span>39</span>
                          </div>
                          <div>
                            <span className="bold">Eyes</span>
                            <br />
                            <span>blue</span>
                          </div>
                          <div>
                            <span className="bold">Hair</span>
                            <br />
                            <span>brunet</span>
                          </div>
                        </div>

                        <a href="model.html" className="theme_button color1">
                          Read More
                        </a>
                        <a
                          href="gallery-fullwidth-4-cols.html"
                          className="theme_button inverse"
                        >
                          View Gallery
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>

          <li>
            <img src="/assets/images/top_slider_01.jpg" alt="" />
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <div className="slide_description_wrapper">
                    <div className="slide_description">
                      <div className="intro-layer" data-animation="expandUp">
                        <h2 className="highlight text-uppercase">Top Models</h2>
                        <h3 className="extra-big text-uppercase">
                          Olivia Torney
                        </h3>
                      </div>
                      <div className="intro-layer" data-animation="expandUp">
                        <div className="model-parameters topmargin_40 bottommargin_40">
                          <div>
                            <span className="bold">Height</span>
                            <br />
                            <span>185</span>
                          </div>
                          <div>
                            <span className="bold">Bust</span>
                            <br />
                            <span>79</span>
                          </div>
                          <div>
                            <span className="bold">Waist</span>
                            <br />
                            <span>59</span>
                          </div>
                          <div>
                            <span className="bold">Hips</span>
                            <br />
                            <span>87</span>
                          </div>
                          <div>
                            <span className="bold">Shoe</span>
                            <br />
                            <span>39</span>
                          </div>
                          <div>
                            <span className="bold">Eyes</span>
                            <br />
                            <span>blue</span>
                          </div>
                          <div>
                            <span className="bold">Hair</span>
                            <br />
                            <span>brunet</span>
                          </div>
                        </div>

                        <a href="model.html" className="theme_button color1">
                          Read More
                        </a>
                        <a
                          href="gallery-fullwidth-4-cols.html"
                          className="theme_button inverse"
                        >
                          View Gallery
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </section>
  );
};

export default Slider;
