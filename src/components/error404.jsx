import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import DocumentMeta from "react-document-meta";
import BreadCrumbs from "./common/breadCrumbs";

const Error404 = (props) => {
  const meta = {
    title: "Bellainti - Page Not Found",
    description: "",
    // canonical: "http://example.com/path/to/page",
    meta: {
      charset: "utf-8",
    },
  };

  useEffect(() => {
    //  props.setBreadCrumbs([{ label: "Home", src: "/" }, { label: "404" }]);
  }, []);

  return (
    <>
      <DocumentMeta {...meta} />
      <BreadCrumbs data={[{ label: "404" }]} />
      <section className="ds section_padding_100">
        <div className="container">
          <div className="row">
            <div className="col-sm-12 text-center">
              <p className="not_found">
                <span className="grey">404</span>
              </p>
              <h2 className="muellerhoff highlight">Oops, page not found!</h2>
              <p>You can browse locations:</p>
              <div className="widget widget_tag_cloud">
                <div className="tagcloud">
                  <Link to="/escorts/nsw" title="NSW Escorts">
                    NSW
                  </Link>
                  <Link to="/escorts/vic" title="VIC Escorts">
                    VIC
                  </Link>
                  <Link to="/escorts/qld" title="QLD Escorts">
                    QLD
                  </Link>
                  <Link to="/escorts/tas" title="TAS Escorts">
                    TAS
                  </Link>
                  <Link to="/escorts/wa" title="WA Escorts">
                    WA
                  </Link>
                  <Link to="/escorts/sa" title="SA Escorts">
                    SA
                  </Link>
                </div>
              </div>
              <p className="divider_20">
                Or
                <br />
              </p>
              <p className="bottommargin_5">
                <Link
                  to="/"
                  className="theme_button inverse"
                  title="Bellainti home page"
                >
                  Go To Home
                </Link>
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Error404;
