import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import DocumentMeta from "react-document-meta";

import _ from "lodash";
import {
  imageFolder,
  meta_title_suffix,
  meta_description_profile_default,
} from "../config.json";
import profile from "../services/profileServices";
import BreadCrumbs from "./common/breadCrumbs";
import PersonalDetails from "./profile/personalDetails";
import Availability from "./profile/availability";
import Touring from "./profile/touring";
import Rates from "./profile/rates";
import Services from "./profile/services";
import VideoGallery from "./profile/videoGallery";
import CtaBtn from "./profile/ctaBtn";
import { getCurrentMember, updateFavourite } from "../services/memberServices";
import GaFire from "./common/gaFire";

import Reviews from "./profile/reviews/reviews";
function Profile(props) {
  let { nickname } = useParams();

  const [meta, setMeta] = useState({});
  const [profileData, setData] = useState();
  const [imagesGallery, setImageGallery] = useState([]);
  const [personalDetails, setPersonalDetails] = useState();
  const [availability, setAvailability] = useState([]);
  const [touring, setTouring] = useState([]);
  const [favourite, setFavourite] = useState(-1);
  const [submitedReview, setSubmitedReview] = useState(0);

  const handleFav = () => {
    try {
      updateFavourite(profileData._id);
      if (favourite === 1) {
        GaFire.remFavourite(profileData.user_id);
        return setFavourite(0);
      }
      GaFire.addFavourite(profileData.user_id);
      setFavourite(1);
    } catch (error) {}
  };

  useEffect(() => {
    const fetchData = async () => {
      if (!nickname) {
        window.location = "/";
      }
      try {
        let response = await profile.fetchProfile(nickname);
        setData(response.data);
      } catch (ex) {
        console.log(ex);
      }
    };

    fetchData();
    window.scrollTo(0, 0);
  }, [nickname]);

  useEffect(() => {
    const setFav = () => {
      const member = getCurrentMember();
      let fav = "";
      if (member && member._id) {
        if (profileData && profileData.favourites) {
          fav = _.findIndex(profileData.favourites, function (o) {
            return o === member._id;
          });
          if (fav === -1) {
            setFavourite(0);
          } else {
            setFavourite(1);
          }
        }
      }
    };

    let photos = [];
    if (
      profileData &&
      profileData.images_gallery &&
      profileData.images_gallery.length > 0
    ) {
      const { images_gallery } = profileData;
      for (let i = 0; i < images_gallery.length; i++) {
        photos.push({
          srcweb: `${imageFolder}/${profileData.user_id}/${images_gallery[i]["thumbnail-web"]}`,
          src: `${imageFolder}/${profileData.user_id}/${images_gallery[i]["thumbnail"]}`,
          width: images_gallery[i].width,
          height: images_gallery[i].height,
        });
      }
      setImageGallery(photos);
    }

    if (profileData && profileData.personal_details) {
      const {
        height,
        age,
        body_type,
        eyes,
        bust,
        dress_size,
        hair_color,
      } = profileData.personal_details;

      let city = "";
      let state = "";
      if (profileData && profileData.contact && profileData.contact.city) {
        city = profileData.contact.city;
      }

      if (profileData && profileData.contact && profileData.contact.state) {
        state = profileData.contact.state;
      }

      setMeta({
        title: `${profileData.nickname} | Escort ${city} - ${state} ${meta_title_suffix}`,
        description:
          profileData.aboutme && profileData.aboutme.shortDescription
            ? profileData.aboutme.shortDescription
            : meta_description_profile_default,
        // canonical: "http://example.com/path/to/page",
        meta: {
          charset: "utf-8",
        },
      });

      let in_call_rates = 0;
      if (
        profileData.rates &&
        profileData.rates.in_call_rates &&
        profileData.rates.in_call_rates.length > 0
      ) {
        in_call_rates = 1;
      }

      let out_call_rates = 0;
      if (
        profileData.rates &&
        profileData.rates.in_call_rates &&
        profileData.rates.in_call_rates.length > 0
      ) {
        out_call_rates = 1;
      }

      setPersonalDetails({
        height,
        age,
        body_type,
        eyes,
        bust,
        dress_size,
        hair_color,
        in_call_rates,
        out_call_rates,
        contact: profileData.contact,
      });
    }

    if (
      profileData &&
      profileData.availability &&
      profileData.availability.period &&
      profileData.availability.period.length > 0
    ) {
      setAvailability(profileData.availability.period);
    }

    if (profileData && profileData.touring) {
      setTouring(profileData.touring);
    }

    if (profileData && profileData.favourites) {
      setFav();
    }

    if (profileData && profileData.user_id) {
      GaFire.profileView(profileData.user_id);
    }
  }, [profileData]);

  const sendAReview = (rate, review) => {
    try {
      profile.postReview(rate, review, profileData._id);
      setSubmitedReview(1);
    } catch (error) {
      setSubmitedReview(-1);
    }
  };

  const BasicRows = () => (
    <>
      <h2>Photo Gallery</h2>
      <section id="photos">
        {imagesGallery.map((item) => (
          <picture>
            <source srcset={item.src} alt={props.nickname} type="image/webp" />
            <source
              srcset={item.srcweb}
              type="image/jpeg"
              alt={props.nickname}
            />
            <img src={item.src} alt={props.nickname} />
          </picture>
        ))}
      </section>
    </>
  );

  return (
    <>
      <DocumentMeta {...meta} />
      {personalDetails ? (
        <BreadCrumbs
          data={[
            { label: "Escorts", src: "/escorts" },
            { label: profileData.nickname },
          ]}
        />
      ) : (
        ""
      )}
      <section className="ds model-page section_padding_70 section_padding_bottom_60 columns_padding_25">
        <div className="container c-mobile-p-top-0">
          <div className="row">
            <div className="col">
              {personalDetails ? (
                <PersonalDetails
                  data={personalDetails}
                  avatar={profileData.avatar}
                  user_id={profileData.user_id}
                  nickname={profileData.nickname}
                  favourite={favourite}
                  availability={availability}
                  handleFav={handleFav}
                />
              ) : (
                ""
              )}
            </div>

            <div className="col-md-7" style={{ paddingLeft: 0, marginTop: 50 }}>
              <div className="row">
                <ul className="nav nav-tabs" role="tablist">
                  <li className="active c-mobile-w-100">
                    <a href="#tabAbout" role="tab" data-toggle="tab">
                      About Me
                    </a>
                  </li>
                  {profileData &&
                  profileData.rates &&
                  (profileData.rates.in_call_rates ||
                    profileData.rates.out_call_rates) ? (
                    <li className="c-mobile-w-100">
                      <a href="#tabRates" role="tab" data-toggle="tab">
                        Rates
                      </a>
                    </li>
                  ) : (
                    ""
                  )}
                  <li className="c-mobile-w-100">
                    <a href="#tabServices" role="tab" data-toggle="tab">
                      Services
                    </a>
                  </li>
                  <li className="c-mobile-w-100">
                    <a href="#tabReviews" role="tab" data-toggle="tab">
                      Reviews
                    </a>
                  </li>
                </ul>

                <div className="tab-content top-color-border">
                  <div className="tab-pane fade in active" id="tabAbout">
                    {profileData &&
                    profileData.aboutme &&
                    profileData.aboutme.description
                      ? profileData.aboutme.description
                          .split(/(\r\n|\n|\r)/gm)
                          .map((value) =>
                            value.toString().length > 0 ? <p>{value}</p> : ""
                          )
                      : ""}
                  </div>
                  {profileData &&
                  profileData.rates &&
                  (profileData.rates.in_call_rates ||
                    profileData.rates.out_call_rates) ? (
                    <div className="tab-pane fade in" id="tabRates">
                      <Rates
                        label="Incall"
                        data={profileData.rates.in_call_rates}
                      />
                      <Rates
                        label="Outcall"
                        data={profileData.rates.out_call_rates}
                      />
                      <CtaBtn
                        data={profileData.contact}
                        user_id={profileData.user_id}
                        classname="c-w-100"
                      />
                    </div>
                  ) : (
                    ""
                  )}
                  <div className="tab-pane fade in" id="tabServices">
                    {profileData &&
                    profileData.personal_details &&
                    profileData.personal_details.categories &&
                    profileData.personal_details.categories.length > 0 ? (
                      <>
                        <Services
                          data={profileData.personal_details.categories}
                        />
                        <CtaBtn
                          data={profileData.contact}
                          user_id={profileData.user_id}
                          classname="c-w-100"
                        />
                      </>
                    ) : (
                      ""
                    )}
                  </div>

                  <div className="tab-pane fade in" id="tabReviews">
                    {profileData ? (
                      <>
                        <Reviews
                          data={profileData.reviews || []}
                          handleAddReview={sendAReview}
                          submitedReview={submitedReview}
                        />
                      </>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
            </div>

            <div
              className="col-md-5 c-bg-black c-p-0"
              style={{ marginTop: 50 }}
            >
              {touring ? <Touring data={touring} /> : ""}
              <Availability data={availability} />
            </div>

            {profileData &&
            profileData.videos_gallery &&
            profileData.videos_gallery.length > 0 ? (
              <div className="col-md-12 c-p-0">
                <VideoGallery
                  data={profileData.videos_gallery}
                  user_id={profileData.user_id}
                />
              </div>
            ) : (
              ""
            )}

            <div className="col-md-12 c-p-0">
              <BasicRows />
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Profile;
