import Cities from "../../services/citiesServices";

export default async function findLocation() {
  try {
    let response = await Cities.fetchCLocation();
    if (response.data.state) {
      return response.data.state;
    }
  } catch (ex) {
    console.log(ex);
    return null;
  }
}
