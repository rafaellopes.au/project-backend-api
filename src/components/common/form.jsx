import React from "react";
import Joi from "joi-browser";

const RenderInput = (props) => {
  return (
    <div class="form-group">
      <label class="">
        {props.label}
        {props.required && props.required === true ? (
          <span class="required">*</span>
        ) : (
          ""
        )}
      </label>
      <input
        type={props.type ? props.type : "text"}
        aria-required="true"
        size="30"
        value={props.value}
        name={props.name}
        id={props.id}
        className="form-control text-center"
        onChange={(e) => props.handleChange(props.id, e.target.value)}
      />
      {props.error ? (
        <span
          className="alert alert-danger"
          style={{ padding: "1px 20px", marginTop: "10px" }}
        >
          {props.error}
        </span>
      ) : (
        ""
      )}
    </div>
  );
};

const RenderTextArea = (props) => {
  return (
    <div class="form-group">
      <label class="">
        {props.label}
        {props.required && props.required === true ? (
          <span class="required">*</span>
        ) : (
          ""
        )}
      </label>
      <textarea
        id={props.id}
        name={props.name}
        rows="4"
        cols="50"
        className="form-control text-center"
        onChange={(e) => props.handleChange(props.id, e.target.value)}
      >
        {props.value}
      </textarea>

      {props.error ? (
        <span
          className="alert alert-danger"
          style={{ padding: "1px 20px", marginTop: "10px" }}
        >
          {props.error}
        </span>
      ) : (
        ""
      )}
    </div>
  );
};

const RenderCheckBox = (props) => {
  return (
    <div class="form-check">
      <input
        type="checkbox"
        class="form-check-input"
        id={props.id}
        Selected={props.value}
      />
      <label class="form-check-label" for={props.id}>
        <span dangerouslySetInnerHTML={{ __html: props.label }} />
      </label>
    </div>
  );
};

const RenderSelect = (props) => {
  return (
    <div class="form-group">
      <label class="">
        {props.label}
        {props.required && props.required === true ? (
          <span class="required">*</span>
        ) : (
          ""
        )}
      </label>
      <select
        name={props.name}
        id={props.id}
        className="form-control text-center"
        value={props.value}
        onChange={(e) => props.handleChange(props.id, e.target.value)}
      >
        {props.options.map((option) => (
          <option value={option.k}>{option.v}</option>
        ))}
      </select>
    </div>
  );
};

const validate = (data, schema) => {
  const options = { abortEarly: false };
  const { error } = Joi.validate(data, schema, options);
  if (!error) return null;

  const errors = {};
  for (let item of error.details) errors[item.path[0]] = item.message;
  return { ...errors };
};

const validateProperty = (name, value, schema) => {
  const obj = { [name]: value };
  const propertyschema = { [name]: schema[name] };
  const { error } = Joi.validate(obj, propertyschema);
  return error ? error.details[0].message : null;
};

export {
  RenderInput,
  RenderCheckBox,
  RenderSelect,
  validate,
  validateProperty,
  RenderTextArea,
};
