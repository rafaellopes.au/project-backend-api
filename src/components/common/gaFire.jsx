import ReactGA from "react-ga";

//Profile
function profileView(user_id) {
  ReactGA.event({
    category: "Profile",
    action: "Viewed_profile",
    label: user_id,
  });
}

function showMobileNumber(user_id) {
  ReactGA.event({
    category: "Profile",
    action: "Show_mobile_number",
    label: user_id,
  });
}

function addFavourite(user_id) {
  ReactGA.event({
    category: "Profile",
    action: "Added_to_favourite",
    label: user_id,
  });
}
function remFavourite(user_id) {
  ReactGA.event({
    category: "Profile",
    action: "Removed_from_favourite",
    label: user_id,
  });
}

function bookingRequest(user_id) {
  ReactGA.event({
    category: "Profile",
    action: "Booking_requested",
    label: user_id,
  });
}

//advertise
function advertiseSignUp() {
  ReactGA.event({
    category: "Advertise",
    action: "Sign_Up",
  });
}

// MEMBERS

function newMember() {
  ReactGA.event({
    category: "Member",
    action: "Member_registered",
  });
}

function activeMemberAccount(member_id) {
  ReactGA.event({
    category: "Member",
    action: "Member_Activated",
    label: member_id,
  });
}

function memberLogin(member_id) {
  ReactGA.event({
    category: "Member",
    action: "Login",
    label: member_id,
  });
}
function memberLogoff(member_id) {
  ReactGA.event({
    category: "Member",
    action: "Logoff",
    label: member_id,
  });
}

function memberUpdateProfile(member_id) {
  ReactGA.event({
    category: "Member",
    action: "Update_profile",
    label: member_id,
  });
}

function memberPasswordChanged(member_id) {
  ReactGA.event({
    category: "Member",
    action: "Password_Changed",
    label: member_id,
  });
}

const gaFire = {
  profileView,
  showMobileNumber,
  addFavourite,
  remFavourite,
  bookingRequest,
  advertiseSignUp,
  newMember,
  activeMemberAccount,
  memberLogin,
  memberLogoff,
  memberUpdateProfile,
  memberPasswordChanged,
};

export default gaFire;
