import React from "react";
import { Link } from "react-router-dom";

const BreadCrumbs = (props) => {
  return (
    <>
      <section className="page_breadcrumbs changeable ls gradient gorizontal_padding section_padding_20 columns_padding_5 table_section">
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-6 text-center">
              <ol className="breadcrumb">
                <li>
                  <Link to="/">Home</Link>
                </li>
                {props.data && props.data.length > 0
                  ? props.data.map((item, index, arr) =>
                      index >= arr.length - 1 ? (
                        <li className="active">
                          <span>{item.label}</span>
                        </li>
                      ) : (
                        <li>
                          <Link to={item.src}>{item.label}</Link>
                        </li>
                      )
                    )
                  : ""}
              </ol>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default BreadCrumbs;
