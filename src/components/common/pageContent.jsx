import React, { useState, useEffect } from "react";
import DocumentMeta from "react-document-meta";

import PageContentService from "../../services/pageContent";
import Config from "../../config.json";

const PageContent = (props) => {
  const [meta, setMeta] = useState();
  const [page, setPage] = useState();

  const handlePageContent = async (slug) => {
    try {
      let content = await PageContentService.fetchPage(slug);
      setPage(content.data);

      let showMeta = {};
      if (content.data.meta_description) {
        showMeta.description = content.data.meta_description;
      } else {
        showMeta.description = Config.default_description;
      }
      if (content.data.meta_title) {
        showMeta.title = content.data.meta_title;
      } else {
        showMeta.title = Config.default_title;
      }
      setMeta(showMeta);
    } catch (ex) {
      console.log("ex", ex);

      let showMeta = {};
      showMeta.title = Config.default_title;
      showMeta.description = Config.default_description;
      setMeta(showMeta);
    }
  };

  useEffect(() => {
    if (!props.slug) {
      return setPage();
    }
    handlePageContent(props.slug);
  }, [props.slug]);

  return (
    <>
      {!page ? (
        ""
      ) : (
        <>
          {meta ? <DocumentMeta {...meta} /> : ""}
          <section className="ds page_models models_square gorizontal_padding section_padding_70 columns_padding_0">
            <div className="container" style={{ paddingBottom: "0px" }}>
              <div className="row">
                <div className="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 text-center">
                  {page && page.title ? (
                    <h1 className="big topmargin_0">{page.title}</h1>
                  ) : (
                    ""
                  )}

                  {page && page.subtitle ? (
                    <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">
                      {page.subtitle}
                    </h2>
                  ) : (
                    ""
                  )}

                  {page && page.content ? (
                    <div dangerouslySetInnerHTML={{ __html: page.content }} />
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </section>
        </>
      )}
    </>
  );
};

export default PageContent;
