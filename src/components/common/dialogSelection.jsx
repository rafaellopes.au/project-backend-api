import React, { useState, useEffect } from "react";
import _ from "lodash";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const DialogSelection = (props) => {
  const [state, setState] = useState(props.selected || []);

  useEffect(() => {
    setState(props.selected);
  }, [props]);

  const handleChange = (event) => {
    const { name } = event.target;

    if (props.multiselect === true) {
      if (_.indexOf(state, name) === -1) {
        setState((state) => [...state, name]);
      } else {
        const newState = _.filter(state, function (o) {
          return o !== name;
        });
        setState(newState);
      }
    } else {
      setState([name]);
    }
  };

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      open={props.openOptions}
      onClose={props.handleCloseOptions}
    >
      <DialogTitle>{props.title}</DialogTitle>
      <DialogContent>
        {props.options
          ? props.options.map((item) => (
              <FormControlLabel
                control={
                  <Checkbox
                    checked={_.indexOf(state, item.v) === -1 ? false : true}
                    onChange={handleChange}
                    name={item.v}
                  />
                }
                label={item.v}
                className={"c-label-col-3"}
              />
            ))
          : ""}
        <FormGroup></FormGroup>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.handleCloseOptions} color="primary">
          Cancel
        </Button>
        <Button
          onClick={() =>
            props.handleCloseOptions({
              data: state,
              type: props.type,
            })
          }
          color="primary"
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogSelection;
