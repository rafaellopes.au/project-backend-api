import React, { useState, useEffect } from "react";
import DocumentMeta from "react-document-meta";
import SearchComponent from "./search/searchComponent";

const Home = (props) => {
  const [defaultSearch] = useState({});

  const meta = {
    title: "Bellainti",
    description: "Bellainti description....",
    // canonical: "http://example.com/path/to/page",
    meta: {
      charset: "utf-8",
    },
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <DocumentMeta {...meta} />
      <section className="ds page_models models_square gorizontal_padding section_padding_70 columns_padding_0">
        <SearchComponent
          default={defaultSearch}
          getLocation={true}
          showFilters={true}
          limitPerPage={10}
        />
      </section>

      <section className="ds parallax calltoaction section_padding_100 parallax-home">
        <div className="container">
          <div className="row topmargin_60 bottommargin_60">
            <div className="col-sm-12 text-center">
              <h2 className="extra-big topmargin_0 bottommargin_30">
                Do You want to be a <span className="highlight">Member?</span>
              </h2>
              <div className="row">
                <div className="col-md-offset-2 col-md-8 text-center">
                  <p className="fontsize_20">
                    If you are 5ft 8in and above women and think you have what
                    it takes to be a model send us headshot and full length shot
                    along with your age, contact details, height, bust, waist
                    and hip measurements.
                  </p>
                </div>
              </div>
              <div className="widget widget_mailchimp topmargin_20">
                <button type="submit" className="btn btn-success">
                  Join Now!
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Home;
