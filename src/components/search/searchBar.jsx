import React, { useState, useEffect } from "react";
import { Link, Redirect, useParams, useHistory } from "react-router-dom";

import _ from "lodash";
import Slider from "@material-ui/core/Slider";
import Button from "@material-ui/core/Button";

import DialogSelection from "../common/dialogSelection";
import options from "../../services/optionServices";
import Cities from "../../services/citiesServices";

async function findLocation() {
  try {
    let response = await Cities.fetchCLocation();
    if (response.data.state) {
      return response.data.state;
    }
  } catch (ex) {
    console.log(ex);
    return "";
  }
}

const SearchBar = (props) => {
  let history = useHistory();
  let { austate: state_param, city: city_param } = useParams();

  useEffect(() => {
    fetchOptionsData();
    fetchCitiesData();
  }, []);

  //options data
  const [optionsData, setOptions] = useState();
  const fetchOptionsData = async () => {
    try {
      let response = await options.fetchOptions();
      setOptions(response.data);
    } catch (ex) {
      console.log(ex);
    }
  };

  //Cities data
  const [citiesData, setCities] = useState();
  const fetchCitiesData = async () => {
    try {
      let response = await Cities.fetchCities();
      setCities(response.data);
    } catch (ex) {
      console.log("ex", ex);
    }
  };

  //Layout
  const [advSearch, setAdvSearch] = useState(true);
  const [byDeviceLocation, setByDeviceLocation] = useState(false);

  //SearchDetails
  const [selections, setSelections] = useState({});

  const handleClickOpenOption = (type, title) => {
    let options = [];
    let multiselect = true;

    if (type === "austate") {
      options = [
        { v: "ACT" },
        { v: "NSW" },
        { v: "QLD" },
        { v: "SA" },
        { v: "TAS" },
        { v: "VIC" },
        { v: "WA" },
      ];
      multiselect = false;
    } else {
      options = _.filter(optionsData, function (o) {
        return o.k === type;
      });

      options = _.sortBy(options, [
        function (o) {
          return o.v;
        },
      ]);
    }

    setOpenOptions({
      open: true,
      options: options,
      selected: selections[type],
      title: title,
      type,
      multiselect,
    });
  };

  const handleChangeSelection = (type, value) => {
    let newSelection = selections;
    newSelection[type] = value;
    setSelections(newSelection);
  };

  const handleCloseOptions = (resp) => {
    if (resp.data) {
      if (
        resp.type === "austate" &&
        resp.data &&
        resp.data.length > 0 &&
        resp.data[0] !== selections.austate
      ) {
        // history.push(`/australia/${resp.data[0].toLowerCase()}`);
      }
      handleChangeSelection(resp.type, resp.data);
    }
    setOpenOptions({ open: false, options: [], selected: [], title: "" });
  };

  const [ageValue, setAgeValue] = useState();
  const [heightValue, setHeightValue] = useState();
  const [dressSizeValue, setDressSizeValue] = useState();
  const [openOptions, setOpenOptions] = useState(false);

  const handleChangeAge = (event, newValue) => {
    setAgeValue(newValue);
  };
  const handleChangeHeight = (event, newValue) => {
    setHeightValue(newValue);
  };
  const handleChangeDressSize = (event, newValue) => {
    setDressSizeValue(newValue);
  };

  useEffect(() => {
    async function processData() {
      let data = {};
      if (heightValue) {
        data.height = heightValue;
      }
      if (ageValue) {
        data.age = ageValue;
      }
      if (selections.body_type && selections.body_type.length > 0) {
        data.body_type = selections.body_type;
      }

      if (
        selections.profile_category &&
        selections.profile_category.length > 0
      ) {
        data.categories = selections.profile_category;
      }
      if (selections.hair_colour && selections.hair_colour.length > 0) {
        data.hair_colour = selections.hair_colour;
      }
      if (selections.eyes && selections.eyes.length > 0) {
        data.eyes = selections.eyes;
      }
      if (selections.bust && selections.bust.length > 0) {
        data.bust = selections.bust;
      }
      if (dressSizeValue) {
        data.dress_size = dressSizeValue;
      }

      if (selections.city) {
        data.city = selections.city;
      } else {
        if (city_param) {
          let newSelection = selections;
          newSelection["city"] = city_param.replaceAll("-", " ");
          setSelections(newSelection);
        }
      }

      if (selections && selections.austate && selections.austate.length > 0) {
        data.state = selections.austate[0];
        props.handleSearchData(data);
      } else {
        if (state_param) {
          let newSelection = selections;
          newSelection["austate"] = [state_param.toUpperCase()];
          return setSelections(newSelection);
          //props.handleSearchData(data);
        } else {
          let state = await findLocation();
          if (state) {
            let varSelections = selections;
            varSelections.austate = [state];
            return setSelections(varSelections);
            //data.state = [state];
            //props.handleSearchData(data);
          } else {
            props.handleSearchData(data);
          }
        }
      }
      // props.handleSearchData(data);
    }
    processData();
  }, [
    ageValue,
    heightValue,
    dressSizeValue,
    selections,
    selections.austate,
    selections.body_type,
    selections.profile_category,
    selections.eyes,
    selections.hair_colour,
    selections.bust,
    selections.city,
  ]);

  useEffect(() => {
    let newSelection = selections;
    if (city_param) {
      newSelection["city"] = city_param.replaceAll("-", " ");
    } else {
      newSelection["city"] = "";
    }

    setSelections(newSelection);
  }, [city_param]);

  return (
    <div className="row bottommargin_50 topmargin_0 boxed-padding">
      <form className="form-inline models-orderby">
        {/*Show by device location*/}
        {byDeviceLocation ? (
          <Redirect to={"/australia/queensland/gold-coast/labrador"} />
        ) : (
          ""
        )}
        <div
          className={`col-lg-12 text-lg-center ${
            byDeviceLocation ? "scaleAppear" : "c-d-none"
          }`}
        >
          <div className="filters isotope_filters inline-block margin_0">
            <span>
              QLD - Gold Coast - Labrador{" "}
              <i
                className="rt-icon2-location-arrow-outline"
                style={{ fontSize: 20 }}
              ></i>
            </span>
            <p
              className="c-underline-over color-green"
              onClick={() => setByDeviceLocation(!byDeviceLocation)}
            >
              Select a diferent location
            </p>
            <hr className="divider-search-bar" />
          </div>
        </div>
        {/*Locations*/}
        <div
          className={`col-lg-12 ${
            byDeviceLocation ? "c-d-none" : "scaleAppear"
          }`}
          style={{ textAlign: "center" }}
        >
          <div className="inline-block margin_0">
            <Button
              onClick={() =>
                handleClickOpenOption("austate", "Select your State")
              }
              className="c-MuiButton"
              style={{
                borderRadius: 0,
                marginRight: 5,
              }}
            >
              State
              {selections.austate && selections.austate.length === 1
                ? `: ${selections.austate[0]}`
                : ""}{" "}
              <i className="rt-icon2-keyboard_arrow_down"></i>
            </Button>
            {selections.austate && selections.austate.length === 1
              ? _.filter(citiesData, function (o) {
                  return o.abbrev === selections.austate[0];
                }).map((austate) =>
                  _.filter(austate.cities, function (o) {
                    return o;
                  }).map((item) => (
                    <Link
                      onClick={() => handleChangeSelection("city", item.city)}
                      className={`theme_button ${
                        selections.city &&
                        selections.city.toLowerCase() ===
                          item.city.toLowerCase()
                          ? ""
                          : "inverse"
                      }`}
                    >
                      {item.city}
                    </Link>
                  ))
                )
              : ""}
          </div>
        </div>
        {/*Search by location*/}
        <div
          className={`col-lg-12 text-lg-center  ${
            byDeviceLocation ? "c-d-none" : "scaleAppear"
          }`}
          style={{ textAlign: "center" }}
        >
          <hr className="divider-search-bar" />

          {/*
          <div className="filters isotope_filters inline-block bottommargin_25">
            <p
              className="btn btn-success cta-location-bt"
              onClick={() => setByDeviceLocation(!byDeviceLocation)}
            >
              Search by your location{" "}
              <i
                className="rt-icon2-location-arrow-outline"
                style={{ fontSize: 20 }}
              ></i>
            </p>
          </div>
         </form>  */}
        </div>
        {/*Selects*/}
        <div
          className={`col-lg-12 text-lg-center ${
            advSearch ? "scaleAppear" : "c-d-none"
          }`}
          style={{ textAlign: "center" }}
        >
          {openOptions ? (
            <DialogSelection
              openOptions={openOptions.open}
              handleCloseOptions={handleCloseOptions}
              title={openOptions.title}
              selected={openOptions.selected || []}
              options={openOptions.options}
              type={openOptions.type}
              multiselect={openOptions.multiselect}
            />
          ) : (
            ""
          )}

          <div
            className="form-group select-group"
            style={{ margin: "0px 10px", minWidth: "0px" }}
          >
            <Button
              onClick={() =>
                handleClickOpenOption("body_type", "Select Body Type:")
              }
              className="c-MuiButton"
            >
              Body Type
              {selections.body_type && selections.body_type.length > 0
                ? ` (${selections.body_type.length})`
                : ""}
            </Button>
          </div>

          <div
            className="form-group select-group"
            style={{ margin: "0px 10px", minWidth: "0px" }}
          >
            <Button
              onClick={() =>
                handleClickOpenOption("profile_category", "Select by Category:")
              }
              className="c-MuiButton"
            >
              Category
              {selections.profile_category &&
              selections.profile_category.length > 0
                ? ` (${selections.profile_category.length})`
                : ""}
            </Button>
          </div>

          <div
            className="form-group select-group"
            style={{ margin: "0px 10px", minWidth: "0px" }}
          >
            <Button
              onClick={() =>
                handleClickOpenOption("eyes", "Select Eyes Colour:")
              }
              className="c-MuiButton"
            >
              Eyes
              {selections.eyes && selections.eyes.length > 0
                ? ` (${selections.eyes.length})`
                : ""}
            </Button>
          </div>

          <div
            className="form-group select-group"
            style={{ margin: "0px 10px", minWidth: "0px" }}
          >
            <Button
              onClick={() =>
                handleClickOpenOption("hair_colour", "Select Hair Colour:")
              }
              className="c-MuiButton"
            >
              Hair Colour
              {selections.hair_colour && selections.hair_colour.length > 0
                ? ` (${selections.hair_colour.length})`
                : ""}
            </Button>
          </div>

          <div
            className="form-group select-group"
            style={{ margin: "0px 10px", minWidth: "0px" }}
          >
            <Button
              onClick={() => handleClickOpenOption("bust", "Select Bust Type:")}
              className="c-MuiButton"
            >
              Bust
              {selections.bust && selections.bust.length > 0
                ? ` (${selections.bust.length})`
                : ""}
            </Button>
          </div>
        </div>
        {/*Sliders*/}
        <div
          className={`col-lg-12 text-lg-center topmargin_25 bottommargin_50 ${
            advSearch ? "scaleAppear" : "c-d-none"
          }`}
        >
          <div className="form-group select-group c-margin-right-20">
            <label className="">Age</label>
            <Slider
              value={ageValue || [18, 60]}
              onChangeCommitted={handleChangeAge}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              min={18}
              max={60}
            />
          </div>
          <div className="form-group select-group  c-margin-right-20">
            <label className="">Height (cm)</label>
            <Slider
              value={heightValue || [130, 200]}
              onChangeCommitted={handleChangeHeight}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              min={130}
              max={200}
            />
          </div>
          <div className="form-group select-group">
            <label className="">Dress Size</label>
            <Slider
              value={dressSizeValue || [1, 20]}
              onChangeCommitted={handleChangeDressSize}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              min={1}
              max={20}
            />
          </div>
        </div>

        <div
          className="col-lg-12 text-lg-center bottommargin_25"
          style={{ textAlign: "center" }}
        >
          {/*<span
            className="c-underline-over"
            onClick={() => setAdvSearch(!advSearch)}
          >
            Advanced Search
            <i
              className={`fa ${advSearch ? "fa-angle-up" : "fa-angle-down"}`}
            ></i>
          </span>*/}
        </div>
      </form>
    </div>
  );
};

export default SearchBar;
