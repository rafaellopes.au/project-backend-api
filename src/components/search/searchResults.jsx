import React from "react";
import ProfilePreview from "./profilePreview";

const SearchResults = (props) => {
  const testTouring = (contact, fromTouring) => {
    let touring = 0;
    if (
      props.filters &&
      props.filters.city &&
      props.filters.city !== "" &&
      contact.city &&
      props.filters.city !== contact.city
    ) {
      touring = 1;
    }

    if (
      props.filters &&
      props.filters.state &&
      contact.state &&
      props.filters.state[0] !== contact.state
    ) {
      touring = 1;
    }

    return touring;
  };
  return (
    <div
      className="isotope_container isotope row masonry-layout bottommargin_20"
      data-filters=".isotope_filters"
    >
      {props.data && props.data.length > 0 ? (
        <>
          {props.data.map((item) => (
            <ProfilePreview
              data={item}
              touring={testTouring(item.contact)}
              member={props.memberresults | null}
            />
          ))}
        </>
      ) : (
        <div class="col-sm-12 text-center">
          {props.memberresults ? (
            <>
              <h2 class="extra-big topmargin_0 bottommargin_30">
                Welcome inside the erotic world of{" "}
                <span class="highlight">Bellainti!</span>
              </h2>
              <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center"></div>
              </div>
            </>
          ) : (
            <>
              <h2 class="extra-big topmargin_0 bottommargin_30">
                Sorry, <span class="highlight">try again!</span>
              </h2>
              <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                  No profiles found for your selection...
                </div>
              </div>
            </>
          )}
        </div>
      )}
    </div>
  );
};

export default SearchResults;
