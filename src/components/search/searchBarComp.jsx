import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import _ from "lodash";
import Slider from "@material-ui/core/Slider";
import Button from "@material-ui/core/Button";

import DialogSelection from "../common/dialogSelection";
import options from "../../services/optionServices";
import Cities from "../../services/citiesServices";

const SearchBarComp = (props) => {
  let { city: city_param } = useParams();

  //basic data ------
  const fetchOptionsData = async () => {
    try {
      let response = await options.fetchOptions();
      setOptions(response.data);
    } catch (ex) {
      console.log(ex);
    }
  };

  const [citiesData, setCities] = useState();
  const [focuscities] = useState([
    {
      state: "ACT",
      cities: ["Canberra"],
    },

    {
      state: "NSW",
      cities: ["Sydney", "Newcastle", "Wollongong", "Albury"],
    },

    {
      state: "QLD",
      cities: [
        "Brisbane",
        "Gold Coast",
        "Sunshine Coast",
        "Cairns",
        "Townsville",
      ],
    },

    {
      state: "SA",
      cities: ["Adelaide"],
    },
    {
      state: "VIC",
      cities: ["Melbourne", "Geelong", "Ballart", "Wodonga"],
    },

    {
      state: "TAS",
      cities: ["Hobart"],
    },

    {
      state: "WA",
      cities: ["Perth", "Mandurah", "Bunbury", "Albany"],
    },
  ]);
  const [allCities, setAllCities] = useState();

  const [optionsData, setOptions] = useState();
  const fetchCitiesData = async () => {
    try {
      let response = await Cities.fetchCities();
      setCities(response.data);
    } catch (ex) {
      console.log("ex", ex);
    }
  };

  useEffect(() => {
    fetchOptionsData();
    fetchCitiesData();
  }, []);

  //end basic data ------

  //Layout
  const [byDeviceLocation] = useState(false);

  //SearchDetails
  const [selections, setSelections] = useState(props.defaultSearch);

  const handleClickOpenOption = (type, title) => {
    let options = [];
    let multiselect = true;

    if (type === "state") {
      options = [
        { v: "ACT" },
        { v: "NSW" },
        { v: "QLD" },
        { v: "SA" },
        { v: "TAS" },
        { v: "VIC" },
        { v: "WA" },
      ];
      multiselect = false;
    } else {
      options = _.filter(optionsData, function (o) {
        return o.k === type;
      });

      options = _.sortBy(options, [
        function (o) {
          return o.v;
        },
      ]);
    }

    setOpenOptions({
      open: true,
      options: options,
      selected: selections[type],
      title: title,
      type,
      multiselect,
    });
  };

  const handleChangeSelection = (type, value) => {
    let newSelection = selections;
    console.log(newSelection);
    if (value.length > 0) {
      newSelection[type] = value;
      if (type === "state") {
        newSelection["city"] = "";
      }
      setSelections(newSelection);
    } else {
      delete newSelection[type];
      setSelections(newSelection);
    }

    props.handleSearchData(newSelection);
  };

  const handleCloseOptions = (resp) => {
    if (resp.data) {
      handleChangeSelection(resp.type, resp.data);
    }
    setOpenOptions({ open: false, options: [], selected: [], title: "" });
  };

  const [ageValue, setAgeValue] = useState();
  const [heightValue, setHeightValue] = useState();
  const [dressSizeValue, setDressSizeValue] = useState();
  const [openOptions, setOpenOptions] = useState(false);

  const handleChangeAge = (event, newValue) => {
    setAgeValue(newValue);
    let updateSelections = selections;
    updateSelections.age = newValue;
    setSelections(updateSelections);
    props.handleSearchData(updateSelections);
  };
  const handleChangeHeight = (event, newValue) => {
    setHeightValue(newValue);
    let updateSelections = selections;
    updateSelections.height = newValue;
    setSelections(updateSelections);
    props.handleSearchData(updateSelections);
  };
  const handleChangeDressSize = (event, newValue) => {
    setDressSizeValue(newValue);
    let updateSelections = selections;
    updateSelections.dress_size = newValue;
    setSelections(updateSelections);
    props.handleSearchData(updateSelections);
  };

  useEffect(() => {
    let newSelection = selections;
    if (city_param) {
      newSelection["city"] = city_param.replaceAll("-", " ");
    } else {
      newSelection["city"] = "";
    }
    setSelections(newSelection);
  }, [city_param, selections]);

  const handleCities = (state) => {
    let showCities = [];
    if (allCities) {
      showCities = _.filter(citiesData, function (o) {
        return o.abbrev === state[0];
      })[0].cities;
    } else {
      showCities = _.filter(focuscities, function (o) {
        return o.state === state[0];
      })[0].cities;
    }
    return showCities.map((item) => (
      <p
        style={{ cursor: "pointer" }}
        onClick={() => handleChangeSelection("city", item)}
        className={`theme_button ${
          selections.city &&
          selections.city.toLowerCase() === item.toLowerCase()
            ? ""
            : "inverse"
        }`}
      >
        {item}
      </p>
    ));
  };

  return (
    <div className="row bottommargin_50 topmargin_0 boxed-padding">
      <form className="form-inline models-orderby">
        {/*Locations*/}
        <div
          className={`col-lg-12 ${
            byDeviceLocation ? "c-d-none" : "scaleAppear"
          }`}
          style={{ textAlign: "center" }}
        >
          <div className="inline-block margin_0">
            <Button
              onClick={() =>
                handleClickOpenOption("state", "Select your State")
              }
              className="c-MuiButton"
              style={{
                borderRadius: 0,
                marginRight: 5,
              }}
            >
              State
              {selections.state && selections.state.length === 1
                ? `: ${selections.state[0]}`
                : ""}{" "}
              <i className="rt-icon2-keyboard_arrow_down"></i>
            </Button>

            {selections.state &&
              selections.state.length === 1 &&
              handleCities(selections.state)}

            <p className="mt-10 c-mb-0">
              {!allCities ? (
                <p
                  className="c-underline-over"
                  onClick={() => setAllCities(true)}
                >
                  Show all cities
                </p>
              ) : (
                <p className="c-underline-over" onClick={() => setAllCities()}>
                  Show less
                </p>
              )}
            </p>
          </div>
        </div>
        {/*Search by location*/}
        <div
          className={`col-lg-12 text-lg-center  ${
            byDeviceLocation ? "c-d-none" : "scaleAppear"
          }`}
          style={{ textAlign: "center" }}
        >
          <hr className="divider-search-bar" />

          {/*
          <div className="filters isotope_filters inline-block bottommargin_25">
            <p
              className="btn btn-success cta-location-bt"
              onClick={() => setByDeviceLocation(!byDeviceLocation)}
            >
              Search by your location{" "}
              <i
                className="rt-icon2-location-arrow-outline"
                style={{ fontSize: 20 }}
              ></i>
            </p>
          </div>
         </form>  */}
        </div>
        {/*Selects*/}
        <div
          className={`col-lg-12 text-lg-center`}
          style={{ textAlign: "center" }}
        >
          {openOptions ? (
            <DialogSelection
              openOptions={openOptions.open}
              handleCloseOptions={handleCloseOptions}
              title={openOptions.title}
              selected={openOptions.selected || []}
              options={openOptions.options}
              type={openOptions.type}
              multiselect={openOptions.multiselect}
            />
          ) : (
            ""
          )}

          <div
            className="form-group select-group homeFormGroup"
            style={{ margin: "0px 10px", minWidth: "0px" }}
          >
            <Button
              onClick={() =>
                handleClickOpenOption("body_type", "Select Body Type:")
              }
              className="c-MuiButton homeSearchBtn"
            >
              Body Type
              {selections.body_type && selections.body_type.length > 0
                ? ` (${selections.body_type.length})`
                : ""}
            </Button>
          </div>

          <div
            className="form-group select-group homeFormGroup"
            style={{ margin: "0px 10px", minWidth: "0px" }}
          >
            <Button
              onClick={() =>
                handleClickOpenOption("profile_category", "Select by Category:")
              }
              className="c-MuiButton homeSearchBtn"
            >
              Category
              {selections.profile_category &&
              selections.profile_category.length > 0
                ? ` (${selections.profile_category.length})`
                : ""}
            </Button>
          </div>

          <div
            className="form-group select-group homeFormGroup"
            style={{ margin: "0px 10px", minWidth: "0px" }}
          >
            <Button
              onClick={() =>
                handleClickOpenOption("eyes", "Select Eyes Colour:")
              }
              className="c-MuiButton homeSearchBtn"
            >
              Eyes
              {selections.eyes && selections.eyes.length > 0
                ? ` (${selections.eyes.length})`
                : ""}
            </Button>
          </div>

          <div
            className="form-group select-group homeFormGroup"
            style={{ margin: "0px 10px", minWidth: "0px" }}
          >
            <Button
              onClick={() =>
                handleClickOpenOption("hair_colour", "Select Hair Colour:")
              }
              className="c-MuiButton homeSearchBtn"
            >
              Hair Colour
              {selections.hair_colour && selections.hair_colour.length > 0
                ? ` (${selections.hair_colour.length})`
                : ""}
            </Button>
          </div>

          <div
            className="form-group select-group homeFormGroup"
            style={{ margin: "0px 10px", minWidth: "0px" }}
          >
            <Button
              onClick={() => handleClickOpenOption("bust", "Select Bust Type:")}
              className="c-MuiButton homeSearchBtn"
            >
              Bust
              {selections.bust && selections.bust.length > 0
                ? ` (${selections.bust.length})`
                : ""}
            </Button>
          </div>
        </div>
        {/*Sliders*/}
        <div
          className={`col-lg-12 text-lg-center topmargin_25 bottommargin_50`}
        >
          <div className="form-group select-group c-margin-right-20 c-mb-30">
            <label className="">Age</label>
            <Slider
              value={ageValue || [18, 60]}
              onChangeCommitted={handleChangeAge}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              min={18}
              max={60}
            />
          </div>
          <div className="form-group select-group  c-margin-right-20 c-mb-30">
            <label className="">Height (cm)</label>
            <Slider
              value={heightValue || [130, 200]}
              onChangeCommitted={handleChangeHeight}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              min={130}
              max={200}
            />
          </div>
          <div className="form-group select-group c-mb-30">
            <label className="">Dress Size</label>
            <Slider
              value={dressSizeValue || [1, 20]}
              onChangeCommitted={handleChangeDressSize}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              min={1}
              max={20}
            />
          </div>
        </div>

        <div
          className="col-lg-12 text-lg-center bottommargin_25 c-mb-30"
          style={{ textAlign: "center" }}
        >
          {/*<span
            className="c-underline-over"
            onClick={() => setAdvSearch(!advSearch)}
          >
            Advanced Search
            <i
              className={`fa ${advSearch ? "fa-angle-up" : "fa-angle-down"}`}
            ></i>
          </span>*/}
        </div>
      </form>
    </div>
  );
};

export default SearchBarComp;
