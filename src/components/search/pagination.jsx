import React, { useState, useEffect } from "react";

const Pagination = (props) => {
  const [pages, setPages] = useState([]);

  useEffect(() => {
    let handlePages = [];
    if (props.data.totalPages === 1) {
      return;
    }
    if (props.data.currentPage > 1) {
      handlePages.push(
        <li>
          <span onClick={() => props.changePage(props.data.currentPage - 1)}>
            <span className="sr-only">Prev</span>
            <i className="rt-icon2-chevron-thin-left"></i>
          </span>
        </li>
      );
    }

    for (let i = 1; i < props.data.totalPages + 1; i++) {
      handlePages.push(
        <li className={`${props.data.currentPage === i ? "active" : ""}`}>
          <span onClick={() => props.changePage(i)}>{i}</span>
        </li>
      );
    }

    if (props.data.currentPage < props.data.totalPages) {
      handlePages.push(
        <li>
          <span onClick={() => props.changePage(props.data.currentPage + 1)}>
            <span className="sr-only">Next</span>
            <i className="rt-icon2-chevron-thin-right"></i>
          </span>
        </li>
      );
    }

    setPages(handlePages);
  }, [props]);

  return (
    <div className="row">
      <div className="col-sm-12 text-center margin_0">
        <ul className="pagination ">{pages.map((item) => item)}</ul>
      </div>
    </div>
  );
};

export default Pagination;
