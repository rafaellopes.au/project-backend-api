import React from "react";
import { Link } from "react-router-dom";
import { imageFolder } from "../../config.json";
import PersonalDetailItem from "../profile/personalDetailItem";

const ProfilePreview = (props) => {
  return (
    <div
      className={`${
        props.member
          ? `col-lg-6 col-md-6 col-sm-6 fashion`
          : `col-lg-3 col-md-4 col-sm-6 fashion`
      }`}
    >
      <Link
        className="p-link"
        title=""
        to={`/escort/profile/${props.data.permalink}`}
      >
        <div className="vertical-item content-absolute">
          <div className="item-media">
            {props.data && props.data.avatar && props.data.avatar.thumbnail ? (
              <picture>
                <source
                  srcset={`${imageFolder}/${props.data.user_id}/${props.data.avatar.thumbnail}`}
                  type="image/webp"
                />
                <source
                  srcset={`${imageFolder}/${props.data.user_id}/${props.data.avatar["thumbnail-web"]}`}
                  type="image/jpeg"
                />
                <img
                  src={`${imageFolder}/${props.data.user_id}/${props.data.avatar["thumbnail-web"]}`}
                  alt="Alt Text!"
                />
              </picture>
            ) : (
              <img
                src="/assets/images/profile_placeholder.png"
                alt=""
                style={{ width: "100%" }}
              />
            )}
            {props.touring !== 0 ? (
              <span
                class={`badge-touring${props.touring === 2 ? "-soon" : ""}`}
              >
                Touring{props.touring === 2 && " Soon"}
              </span>
            ) : (
              ""
            )}
            <div className="media-links"></div>
          </div>
          <div className="item-content text-center before_cover cs">
            <div className="links-wrap">
              <Link
                className="p-link"
                to={`/escort/profile/${props.data.permalink}`}
              >
                {props.data.nickname}
              </Link>
            </div>
            <div className="bg_overlay"></div>
            <div className="model-parameters">
              {props.data &&
              props.data.contact &&
              props.data.contact.city &&
              props.data.contact.state ? (
                <div style={{ display: "block" }}>
                  {props.data.contact.city}, {props.data.contact.state}
                </div>
              ) : (
                ""
              )}
              <PersonalDetailItem
                label="Age"
                value={props.data.personal_details.age}
                col="col-md-4"
              />
              <PersonalDetailItem
                label="Height"
                value={props.data.personal_details.height}
                unit="cm"
                col="col-md-4"
              />
              <PersonalDetailItem
                label="Body Type"
                value={props.data.personal_details.body_type}
                col="col-md-4"
              />
              <PersonalDetailItem
                label="Eyes"
                value={props.data.personal_details.eyes}
                col="col-md-4"
              />
              <PersonalDetailItem
                label="Bust"
                value={props.data.personal_details.bust}
                col="col-md-4"
              />

              <PersonalDetailItem
                label="Hair Color"
                value={props.data.personal_details.hair_color}
                col="col-md-4"
              />
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default ProfilePreview;
