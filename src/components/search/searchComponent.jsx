import React, { useState, useEffect } from "react";

import SearchBarComp from "../search/searchBarComp";
import SearchResults from "../search/searchResults";
import Pagination from "../search/pagination";
import findLocation from "../common/findLocation";
import profile from "../../services/profileServices";

const SearchComponent = (props) => {
  const [profilesList, setProfileList] = useState();
  const [defaultSearch, setDefaultSearch] = useState();
  const [pagination, setPagination] = useState();
  const [currentPage, setCurrentPage] = useState(1);

  const fetchSearchProfiles = async (data) => {
    try {
      let response = await profile.fetchSearch(
        data,
        currentPage,
        props.limitPerPage
      );
      setProfileList(response.data.profiles);
      setPagination({
        currentPage: response.data.currentPage,
        totalPages: response.data.totalPages,
      });
    } catch (ex) {
      console.log(ex);
    }
  };

  useEffect(() => {
    let default_search = props.default;

    const doAsync = async () => {
      if (props.getLocation) {
        const foundState = await findLocation();
        if (foundState) {
          default_search.state = [foundState];
        }
      }
    };
    doAsync();

    fetchSearchProfiles(default_search);
    setDefaultSearch(default_search);
  }, [props]);

  const handleSearchData = (data) => {
    fetchSearchProfiles(data);
    setDefaultSearch(data);
  };

  const handleChangePage = (page) => {
    setCurrentPage(page);
  };

  useEffect(() => {
    fetchSearchProfiles(defaultSearch);
  }, [currentPage]);

  return (
    <>
      <div className="container-fluid">
        {defaultSearch && props.showFilters ? (
          <SearchBarComp
            handleSearchData={handleSearchData}
            defaultSearch={defaultSearch}
            {...props}
          />
        ) : (
          ""
        )}
        {profilesList ? (
          <SearchResults data={profilesList} filters={defaultSearch} />
        ) : (
          "Loading..."
        )}
        {pagination && (
          <Pagination data={pagination} changePage={handleChangePage} />
        )}
      </div>
    </>
  );
};

export default SearchComponent;
