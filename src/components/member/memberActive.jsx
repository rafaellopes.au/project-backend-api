import React, { useState, useEffect } from "react";
import { active, getCurrentMember } from "../../services/memberServices";
import { useParams } from "react-router-dom";
import gaFire from "../common/gaFire";

const MemberActive = () => {
  const { id } = useParams();
  const [validated, setValidated] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  useEffect(() => {
    const activeMember = async () => {
      try {
        const member = await active(id);
        localStorage.setItem("tokenKey", member.data);
        gaFire.activeMemberAccount(getCurrentMember()._id);
        window.location = "/member/profile";
      } catch (ex) {
        setValidated(true);
        if (ex.response && ex.response.data) {
          setErrorMsg(ex.response.data);
        } else {
          setErrorMsg("Internal Error!");
        }
      }
    };
    activeMember();
  }, [id]);

  return (
    <>
      <section class="ds section_padding_70">
        <div class="container">
          <div class="row">
            <div class={`col-lg-12 text-center ${!validated ? "d-none" : ""}`}>
              <h2 class="big margin_0">Member</h2>
              <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">
                Faild!
              </h2>
              <div>{errorMsg}</div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default MemberActive;
