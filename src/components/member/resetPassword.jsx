import React, { useState, useEffect } from "react";
import Joi from "joi-browser";
import { Link, useParams } from "react-router-dom";
import { resetPassword } from "../../services/memberServices";

import { RenderInput, validateProperty, validate } from "../common/form";

const ResetPassword = () => {
  const { id } = useParams();
  const [formData, setFormData] = useState({
    password: "",
    confirmPassword: "",
    recovery_id: id,
  });

  const [completed, setCompleted] = useState(false);
  const schema = {
    password: Joi.string().min(8).max(255).required().label("Password"),
    confirmPassword: Joi.string()
      .min(8)
      .max(255)
      .required()
      .label("Confirm Password"),
    recovery_id: Joi.string().required(),
  };

  const [error, setError] = useState({});
  const [processing, setProcessing] = useState(false);

  const handleChange = (k, v) => {
    let newData = { ...formData };
    newData[k] = v;
    setFormData(newData);
    const validate = validateProperty(k, v, schema);

    let newError = { ...error };
    newError[k] = validate;
    setError(newError);
  };

  const processSubmit = async () => {
    if (formData.password !== formData.confirmPassword) {
      setError({
        submit: "Confirm password doesn't match with the password.",
      });
      setProcessing(false);
      setTimeout(() => {
        setError({});
      }, 3000);
      return null;
    }

    let format_formData = formData;
    delete format_formData.confirmPassword;
    try {
      await resetPassword(format_formData);
      setCompleted(true);
    } catch (ex) {
      setProcessing(false);
      console.log("ex", ex);
      if (ex.response && ex.response.status === 400) {
        const errors = { ...error };
        errors.submit = ex.response.data;
        setError({ ...errors });
      }
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const errors = validate(formData, schema);
    if (errors) {
      console.log(errors);
      setProcessing(false);
      return setError(errors);
    }
    console.log("---1");
    processSubmit();
  };

  useEffect(() => {
    if (localStorage.getItem("tokenKey")) {
      window.location = "/member/profile";
    }
  }, []);

  return (
    <>
      <section class="ds section_padding_70">
        <div class="container">
          <div class="row">
            <div class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6 text-center">
              <h2 class="big margin_0">Reset Password</h2>
              <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">
                {" "}
              </h2>
              <form
                class="contact-form"
                onSubmit={handleSubmit}
                className={completed === true ? "d-none" : "w-50"}
              >
                <RenderInput
                  label="Password"
                  required="true"
                  value={formData.password}
                  error={error.password}
                  name="password"
                  id="password"
                  type="password"
                  handleChange={handleChange}
                />

                <RenderInput
                  label="Confirm Password"
                  required="true"
                  value={formData.confirmPassword}
                  error={error.confirmPassword}
                  name="confirmPassword"
                  id="confirmPassword"
                  type="password"
                  handleChange={handleChange}
                />

                {!processing ? (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    class="theme_button color1"
                  >
                    Reset
                  </button>
                ) : (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    disabled
                  >
                    Processing...
                  </button>
                )}
                {error && error.submit ? (
                  <span
                    className="alert alert-danger d-block"
                    style={{
                      padding: "1px 20px",
                      marginTop: "10px",
                      display: "block",
                    }}
                  >
                    {error.submit}
                  </span>
                ) : (
                  ""
                )}
              </form>

              <div className={completed === false ? "d-none" : ""}>
                Your password has been updated. Click{" "}
                <Link to="/member">here</Link> to login.
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ResetPassword;
