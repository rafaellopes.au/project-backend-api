import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import {
  fetchMemberFavourites,
  getCurrentMember,
} from "../../services/memberServices";
import SearchResults from "../search/searchResults";
import MemberUpdate from "./memberUpdate";
import MemberUpdatePassword from "./memberUpdatePassword";
import BreadCrumbs from "../common/breadCrumbs";
import gaFire from "../common/gaFire";

const MemberProfile = () => {
  const [favourites, setFavourites] = useState([]);
  const fetchFavourites = async () => {
    try {
      const fav = await fetchMemberFavourites();
      setFavourites(fav.data);
    } catch (ex) {
      console.log(ex);
    }
  };

  useEffect(() => {
    fetchFavourites();
  }, []);

  const logOff = () => {
    gaFire.memberLogoff(getCurrentMember()._id);
    localStorage.removeItem("tokenKey");
    window.location = "/member";
  };

  return (
    <>
      <BreadCrumbs data={[{ src: "#", label: "Members" }]} />
      <section className="ds page_models models_square gorizontal_padding section_padding_70 columns_padding_0">
        <div className="container" style={{ paddingBottom: 0 }}>
          <div className="row">
            <div className="col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 text-center">
              <h2 className="big topmargin_0">Members Panel</h2>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-12">
              <div className="row vertical-tabs">
                <div className="col-sm-4">
                  <ul className="nav" role="tablist">
                    <li className="active">
                      <a href="#vertical-tab1" role="tab" data-toggle="tab">
                        <i className="rt-icon2-heart3"></i> Favourites
                      </a>
                    </li>
                    <li>
                      <a href="#vertical-tab2" role="tab" data-toggle="tab">
                        <i className="rt-icon2-pencil"></i> Update profile
                      </a>
                    </li>
                    <li>
                      <a href="#vertical-tab3" role="tab" data-toggle="tab">
                        <i className="rt-icon2-key-outline"></i> Change Password
                      </a>
                    </li>
                    <li>
                      <Link
                        to="#vertical-tab3"
                        role="tab"
                        data-toggle="tab"
                        onClick={logOff}
                      >
                        <i className="rt-icon2-key-outline"></i> Log off
                      </Link>
                    </li>
                  </ul>
                </div>

                <div className="col-sm-8">
                  <div
                    className="tab-content"
                    style={{
                      background: "none",
                      padding: "9px 30px",
                    }}
                  >
                    <div className="tab-pane fade in active" id="vertical-tab1">
                      {favourites ? (
                        <SearchResults data={favourites} memberresults={true} />
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="tab-pane fade" id="vertical-tab2">
                      <MemberUpdate />
                    </div>
                    <div className="tab-pane fade" id="vertical-tab3">
                      <MemberUpdatePassword />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default MemberProfile;
