import React, { useState, useEffect } from "react";
import Joi from "joi-browser";
import { register } from "../../services/memberServices";
import MemberLogin from "./memberLogin";
import BreadCrumbs from "../common/breadCrumbs";

import { RenderInput, validateProperty, validate } from "../common/form";
import gaFire from "../common/gaFire";

const Member = () => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    mobile: "",
    password: "",
    confirmPassword: "",
  });

  const [completed, setCompleted] = useState(false);
  const schema = {
    name: Joi.string().min(2).max(255).required().label("Name"),
    email: Joi.string().min(5).max(255).required().email().label("Email"),
    mobile: Joi.string().allow(null, "").min(10).max(10).label("Mobile Number"),
    password: Joi.string().min(8).max(255).required().label("Password"),
    confirmPassword: Joi.string()
      .min(8)
      .max(255)
      .required()
      .label("Confirm Password"),
  };

  const [error, setError] = useState({});
  const [processing, setProcessing] = useState(false);

  const handleChange = (k, v) => {
    let newData = { ...formData };
    newData[k] = v;
    setFormData(newData);
    const validate = validateProperty(k, v, schema);

    let newError = { ...error };
    newError[k] = validate;
    setError(newError);
  };

  const processSubmit = async () => {
    if (formData.password !== formData.confirmPassword) {
      setError({
        submit: "Confirm password doesn't match with the password.",
      });
      setProcessing(false);
      setTimeout(() => {
        setError({});
      }, 3000);
      return null;
    }

    let format_formData = formData;
    delete format_formData.confirmPassword;
    try {
      await register(format_formData);
      setCompleted(true);
      gaFire.newMember();
    } catch (ex) {
      setProcessing(false);
      console.log("ex", ex);
      if (ex.response && ex.response.status === 400) {
        const errors = { ...error };
        errors.submit = ex.response.data;
        setError({ ...errors });
      }
    }
  };

  const handleSubmit = (e) => {
    setProcessing(true);
    e.preventDefault();
    const errors = validate(formData, schema);
    if (errors) {
      setProcessing(false);
      return setError(errors);
    }

    processSubmit();
  };

  useEffect(() => {
    if (localStorage.getItem("tokenKey")) {
      window.location = "/member/profile";
    }
  }, []);

  return (
    <>
      <BreadCrumbs data={[{ src: "#", label: "Members" }]} />
      <section class="ds section_padding_70">
        <div class="container">
          <div class="row">
            <div style={{ display: "flex" }}>
              <div class="text-center">
                <h2 class="big margin_0">Become an Esteemed Member</h2>
                <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">
                  No more flaky bookings, miscommunication or unprofessional
                  encounters.
                </h2>
                <div>
                  <p>
                    Unlike other member sites, the Penthouse member experience
                    is designed for both efficiency and integrity, and our
                    concierge technology is ready to court you closer to your
                    climax.
                  </p>
                  <p>
                    Take out the guesswork by keeping all your favourites in one
                    place and setting up your private member area to streamline
                    your search, whilst you sit back and enjoy the same top
                    shelf service you’re used to.
                  </p>
                  <p>
                    Plus, waste no time getting what you want, via instant
                    booking, escort + client reviews, tour notifications, plus
                    everything you need to secure an experience you can rely on.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div className="col-md-6 text-center">
              <h2 class="big" style={{ marginTop: "50px" }}>
                Start now
              </h2>
              <form
                class=""
                onSubmit={handleSubmit}
                className={completed === true ? "d-none" : ""}
              >
                <RenderInput
                  label="Display Name*"
                  required="true"
                  value={formData.name}
                  error={error.name}
                  name="name"
                  id="name"
                  handleChange={handleChange}
                />

                <RenderInput
                  label="Email*"
                  required="true"
                  value={formData.email}
                  error={error.email}
                  name="email"
                  id="email"
                  handleChange={handleChange}
                />

                <RenderInput
                  label="Mobile Number"
                  required="false"
                  value={formData.mobile}
                  error={error.mobile}
                  name="mobile"
                  id="mobile"
                  handleChange={handleChange}
                />

                <RenderInput
                  label="Password*"
                  required="true"
                  value={formData.password}
                  error={error.password}
                  name="password"
                  id="password"
                  type="password"
                  handleChange={handleChange}
                />

                <RenderInput
                  label="Confirm Password*"
                  required="true"
                  value={formData.confirmPassword}
                  error={error.confirmPassword}
                  name="confirmPassword"
                  id="confirmPassword"
                  type="password"
                  handleChange={handleChange}
                />

                {!processing ? (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    class="theme_button color1"
                  >
                    Submit
                  </button>
                ) : (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    disabled
                  >
                    Processing...
                  </button>
                )}
                {error && error.submit ? (
                  <span
                    className="alert alert-danger d-block"
                    style={{
                      padding: "1px 20px",
                      marginTop: "10px",
                      display: "block",
                    }}
                  >
                    {error.submit}
                  </span>
                ) : (
                  ""
                )}
              </form>

              <div
                className={completed === false ? "d-none" : "success-msg c-m-0"}
              >
                Your member registration has been completed! Please check your
                email and confirm your registration.
              </div>
            </div>

            <div className="col-md-6">
              <MemberLogin />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Member;
