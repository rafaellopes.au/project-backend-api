import React, { useState } from "react";
import Joi from "joi-browser";
import RequestPassword from "./requestPassword";
import { Link } from "react-router-dom";
import { auth, getCurrentMember } from "../../services/memberServices";
import { RenderInput, validateProperty, validate } from "../common/form";
import gaFire from "../common/gaFire";

const MemberLogin = () => {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const schema = {
    email: Joi.string().min(5).max(255).required().email().label("Email"),
    password: Joi.string().min(8).max(255).required().label("Password"),
  };

  const [error, setError] = useState({});
  const [processing, setProcessing] = useState(false);

  const handleChange = (k, v) => {
    let newData = { ...formData };
    newData[k] = v;
    setFormData(newData);
    const validate = validateProperty(k, v, schema);

    let newError = { ...error };
    newError[k] = validate;
    setError(newError);
  };

  const processSubmit = async () => {
    let format_formData = formData;
    try {
      const member = await auth(format_formData);
      localStorage.setItem("tokenKey", member.data);
      gaFire.memberLogin(getCurrentMember()._id);
      window.location = "/member/profile";
    } catch (ex) {
      setProcessing(false);
      console.log("ex", ex);
      if (ex.response && ex.response.status === 400) {
        const errors = { ...error };
        errors.submit = ex.response.data;
        setError({ ...errors });
      }
    }
  };

  const handleSubmit = (e) => {
    setProcessing(true);
    e.preventDefault();
    const errors = validate(formData, schema);
    if (errors) {
      setProcessing(false);
      return setError(errors);
    }

    processSubmit();
  };

  const [reqPass, setReqPass] = useState(false);

  return (
    <div style={{ marginTop: "40px" }}>
      <div class="contact-form text-center">
        <h2 class="big margin_0">Login</h2>
        <h2 class="muellerhoff topmargin_5 bottommargin_50 highlight">
          Are you a member?
        </h2>
        <form class={`${reqPass ? "d-none" : ""}`} onSubmit={handleSubmit}>
          <RenderInput
            label="Email"
            required="true"
            value={formData.email}
            error={error.email}
            name="email"
            id="email"
            handleChange={handleChange}
          />

          <RenderInput
            label="Password"
            required="true"
            value={formData.password}
            error={error.password}
            name="password"
            id="password"
            type="password"
            handleChange={handleChange}
          />

          {!processing ? (
            <button
              id="contact_form_submit"
              name="contact_submit"
              class="theme_button color1"
            >
              Login
            </button>
          ) : (
            <button id="contact_form_submit" name="contact_submit" disabled>
              Processing...
            </button>
          )}
          {error && error.submit ? (
            <span
              className="alert alert-danger d-block"
              style={{
                padding: "1px 20px",
                marginTop: "10px",
                display: "block",
              }}
            >
              {error.submit}
            </span>
          ) : (
            ""
          )}
          <p style={{ marginTop: "15px" }}>
            Forgot your password?{" "}
            <a onClick={(e) => setReqPass(true)}>Click here</a>
          </p>
        </form>

        {reqPass ? <RequestPassword /> : ""}
      </div>
    </div>
  );
};

export default MemberLogin;
