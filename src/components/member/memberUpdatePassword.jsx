import React, { useState } from "react";
import Joi from "joi-browser";
import { Link } from "react-router-dom";
import {
  updatePassword,
  getCurrentMember,
} from "../../services/memberServices";
import gaFire from "../common/gaFire";

import { RenderInput, validateProperty, validate } from "../common/form";

const MemberUpdatePassword = () => {
  const [formData, setFormData] = useState({
    password: "",
    newpassword: "",
    confirmnewpassword: "",
  });

  const [completed, setCompleted] = useState(false);
  const schema = {
    password: Joi.string().min(8).max(255).required().label("Password"),
    newpassword: Joi.string().min(8).max(255).required().label("New Password"),
    confirmnewpassword: Joi.string()
      .min(8)
      .max(255)
      .required()
      .label("Confirm New Password"),
  };

  const [error, setError] = useState({});
  const [processing, setProcessing] = useState(false);

  const handleChange = (k, v) => {
    let newData = { ...formData };
    newData[k] = v;
    setFormData(newData);
    const validate = validateProperty(k, v, schema);

    let newError = { ...error };
    newError[k] = validate;
    setError(newError);
  };

  const processSubmit = async () => {
    if (formData.newpassword !== formData.confirmnewpassword) {
      setError({
        submit: "Confirm password doesn't match with the new password.",
      });
      setProcessing(false);
      setTimeout(() => {
        setError({});
      }, 3000);
      return null;
    }
    let format_formData = formData;
    delete format_formData.confirmnewpassword;
    try {
      await updatePassword(format_formData);
      setCompleted(true);
      gaFire.memberPasswordChanged(getCurrentMember()._ids);
    } catch (ex) {
      setProcessing(false);
      if (ex.response && ex.response.status === 400) {
        const errors = { ...error };
        errors.submit = ex.response.data;
        setError({ ...errors });
        setFormData({
          password: "",
          newpassword: "",
          confirmnewpassword: "",
        });
        setTimeout(() => {
          setError({});
        }, 3000);
      }
    }
  };

  const handleSubmit = (e) => {
    setProcessing(true);
    e.preventDefault();
    const errors = validate(formData, schema);
    if (errors) {
      setProcessing(false);
      return setError(errors);
    }
    processSubmit();
  };

  const reload = () => {
    window.location = "/member/profile";
  };
  return (
    <>
      <section class="ds section_padding_70">
        <div class="container pt-0">
          <div class="row">
            <div class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6 text-center">
              <h2 class="big margin_0">Update pasword</h2>
              <p>&nbsp;</p>
              <form
                class="contact-form"
                onSubmit={handleSubmit}
                className={completed === true ? "d-none" : ""}
              >
                <RenderInput
                  label="Current Password"
                  required="true"
                  value={formData.password}
                  error={error.password}
                  name="password"
                  id="password"
                  type="password"
                  handleChange={handleChange}
                />

                <RenderInput
                  label="New Password"
                  required="true"
                  value={formData.newpassword}
                  error={error.newpassword}
                  name="newpassword"
                  id="newpassword"
                  type="password"
                  handleChange={handleChange}
                />

                <RenderInput
                  label="Confirm New Password"
                  required="true"
                  value={formData.confirmnewpassword}
                  error={error.confirmnewpassword}
                  name="confirmnewpassword"
                  id="confirmnewpassword"
                  type="password"
                  handleChange={handleChange}
                />

                {!processing ? (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    class="theme_button color1"
                  >
                    Update Password
                  </button>
                ) : (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    disabled
                  >
                    Processing...
                  </button>
                )}
                {error && error.submit ? (
                  <span
                    className="alert alert-danger d-block"
                    style={{
                      padding: "1px 20px",
                      marginTop: "10px",
                      display: "block",
                    }}
                  >
                    {error.submit}
                  </span>
                ) : (
                  ""
                )}
              </form>

              <div className={completed === false ? "d-none" : ""}>
                Your profile has been updated!
                <br />
                <Link onClick={reload}>Go back</Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default MemberUpdatePassword;
