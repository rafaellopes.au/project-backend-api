import React, { useState, useEffect } from "react";
import Joi from "joi-browser";
import { Link } from "react-router-dom";
import {
  updateProfile,
  fetchMember,
  getCurrentMember,
} from "../../services/memberServices";

import { RenderInput, validateProperty, validate } from "../common/form";
import gaFire from "../common/gaFire";

const MemberUpdate = () => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    mobile: "",
    password: "",
  });

  const [completed, setCompleted] = useState(false);
  const schema = {
    name: Joi.string().min(2).max(255).required().label("Name"),
    email: Joi.string().min(5).max(255).required().email().label("Email"),
    mobile: Joi.string().allow("").min(10).max(10).label("MObile Number"),
    password: Joi.string().max(255).required().label("Password"),
  };

  const [error, setError] = useState({});
  const [processing, setProcessing] = useState(false);

  const handleChange = (k, v) => {
    let newData = { ...formData };
    newData[k] = v;
    setFormData(newData);
    const validate = validateProperty(k, v, schema);

    let newError = { ...error };
    newError[k] = validate;
    setError(newError);
  };

  const processSubmit = async () => {
    let format_formData = formData;
    try {
      await updateProfile(format_formData);
      setCompleted(true);
      gaFire.memberUpdateProfile(getCurrentMember()._id);
    } catch (ex) {
      setProcessing(false);
      console.log("ex", ex);
      if (ex.response && ex.response.status === 400) {
        const errors = { ...error };
        errors.submit = ex.response.data;
        setError({ ...errors });
        setTimeout(() => {
          setError({});
        }, 3000);
      }
    }
  };

  const handleSubmit = (e) => {
    setProcessing(true);
    e.preventDefault();
    const errors = validate(formData, schema);
    if (errors) {
      setProcessing(false);
      return setError(errors);
    }
    processSubmit();
  };

  const fetchData = async () => {
    try {
      const datamember = await fetchMember();
      setFormData({
        name: datamember.data.name,
        email: datamember.data.email,
        mobile: datamember.data.mobile,
        password: "",
      });
    } catch (ex) {
      console.log(ex);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const reload = () => {
    window.location = "/member/profile";
  };
  return (
    <>
      <section class="ds section_padding_70">
        <div class="container pt-0">
          <div class="row">
            <div class="contact-form text-center">
              <h2 class="big margin_0">Update profile</h2>
              <p>&nbsp;</p>
              <form
                class="contact-form"
                onSubmit={handleSubmit}
                className={completed === true ? "d-none" : ""}
              >
                <RenderInput
                  label="Display Name*"
                  required="true"
                  value={formData.name}
                  error={error.name}
                  name="name"
                  id="name"
                  handleChange={handleChange}
                />

                <RenderInput
                  label="Email*"
                  required="true"
                  value={formData.email}
                  error={error.email}
                  name="email"
                  id="email"
                  handleChange={handleChange}
                />
                <RenderInput
                  label="Mobile Number"
                  required="false"
                  value={formData.mobile}
                  error={error.mobile}
                  name="mobile"
                  id="mobile"
                  handleChange={handleChange}
                />

                <RenderInput
                  label="Confirm Your Password*"
                  required="true"
                  value={formData.password}
                  error={error.password}
                  name="password"
                  id="password"
                  type="password"
                  handleChange={handleChange}
                />

                {!processing ? (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    class="theme_button color1"
                  >
                    Update Profile
                  </button>
                ) : (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    disabled
                  >
                    Processing...
                  </button>
                )}
                {error && error.submit ? (
                  <span
                    className="alert alert-danger d-block"
                    style={{
                      padding: "1px 20px",
                      marginTop: "10px",
                      display: "block",
                    }}
                  >
                    {error.submit}
                  </span>
                ) : (
                  ""
                )}
              </form>

              <div className={completed === false ? "d-none" : ""}>
                Your password has been updated!
                <br />
                <Link onClick={reload}>Go back</Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default MemberUpdate;
