import React, { useState } from "react";
import Joi from "joi-browser";
import { requestPasswordRestore } from "../../services/memberServices";

import { RenderInput, validateProperty, validate } from "../common/form";

const RequestPassword = () => {
  const [formData, setFormData] = useState({
    email: "",
  });

  const [completed, setCompleted] = useState(false);
  const schema = {
    email: Joi.string().min(5).max(255).required().email().label("Email"),
  };

  const [error, setError] = useState({});
  const [processing, setProcessing] = useState(false);

  const handleChange = (k, v) => {
    let newData = { ...formData };
    newData[k] = v;
    setFormData(newData);
    const validate = validateProperty(k, v, schema);

    let newError = { ...error };
    newError[k] = validate;
    setError(newError);
  };

  const processSubmit = async () => {
    let format_formData = formData;
    try {
      await requestPasswordRestore(format_formData);
      setCompleted(true);
    } catch (ex) {
      setProcessing(false);
      console.log("ex", ex);
      if (ex.response && ex.response.status === 400) {
        const errors = { ...error };
        errors.submit = ex.response.data;
        setError({ ...errors });
      }
    }
  };

  const handleSubmit = (e) => {
    setProcessing(true);
    e.preventDefault();
    const errors = validate(formData, schema);
    if (errors) {
      setProcessing(false);
      return setError(errors);
    }

    processSubmit();
  };

  return (
    <>
      <section class="ds section_padding_70">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2>Reset Password</h2>
              <form
                class={completed === true ? "d-none" : ""}
                onSubmit={handleSubmit}
                className={completed === true ? "d-none" : ""}
              >
                <RenderInput
                  label="Your Email"
                  required="true"
                  value={formData.email}
                  error={error.email}
                  name="email"
                  id="email"
                  handleChange={handleChange}
                />

                {!processing ? (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    class="theme_button color1"
                  >
                    Send Email
                  </button>
                ) : (
                  <button
                    id="contact_form_submit"
                    name="contact_submit"
                    disabled
                  >
                    Processing...
                  </button>
                )}
                {error && error.submit ? (
                  <span
                    className="alert alert-danger d-block"
                    style={{
                      padding: "1px 20px",
                      marginTop: "10px",
                      display: "block",
                    }}
                  >
                    {error.submit}
                  </span>
                ) : (
                  ""
                )}
              </form>

              <div
                className={completed === false ? "d-none" : "success-msg c-m-0"}
              >
                You should receive an email shortly with instructions on how to
                reset your password.
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default RequestPassword;
