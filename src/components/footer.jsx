import React from "react";
import { Link } from "react-router-dom";

function Footer(props) {
  const year = () => {
    var d = new Date();
    return d.getFullYear();
  };
  return (
    <>
      <footer className="ds page_footer section_padding_70">
        <div className="container">
          <div className="row">
            <div className="col-md-4 col-lg-4" data-animation="scaleAppear">
              <div className="widget widget_text">
                <a href="./" className="logo logo_image bottommargin_10">
                  <img src="/assets/images/logo.png" alt="" />
                </a>
                <p>
                  The only platform designed to elevate independent escorts and
                  enhance their wellbeing, via integrated entrepreneurial
                  technology.
                </p>
              </div>
            </div>
            <div className="col-md-8 col-lg-8" data-animation="scaleAppear">
              <div className="row footer_lists">
                <div className="col-md-4">
                  <h3 className="widget-title">Navigation</h3>
                  <div>
                    <ul className="list2 bottommargin_0">
                      <li className="">
                        <Link to={"/"}>Homepage</Link>
                      </li>
                      <li className="">
                        <Link to={"/"}>Locations</Link>
                      </li>

                      <li className="">
                        <Link to={"/"}>Categries</Link>
                      </li>
                      <li className="">
                        <Link to={"/"}>Advertise</Link>
                      </li>
                      <li className="">
                        <Link to={"/"}>Members</Link>
                      </li>
                      <li className="">
                        <Link to={"/"}>Contact</Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-md-4">
                  <h3 className="widget-title">For Members</h3>
                  <div>
                    <ul className="list2 bottommargin_0">
                      <li className="">
                        <Link to={"/member"}>Become a Member</Link>
                      </li>
                      <li className="">
                        <Link to={"/member"}>Login</Link>
                      </li>

                      <li className="">
                        <Link to={"/member/profile"}>Favourites</Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-md-4">
                  <h3 className="widget-title">For Escorts</h3>
                  <div>
                    <ul className="list2 bottommargin_0">
                      <li className="">
                        <Link to={"/advertise"}>Place an Ad</Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>

      <section className="ls page_copyright section_padding_20">
        <div className="container">
          <div className="row topmargin_5 bottommargin_5">
            <div className="col-sm-12 text-center">
              <p className="darklinks">
                &copy; Bellainti {year()} | Created with
                <i className="rt-icon2-heart highlight"></i> by{" "}
                <Link
                  to="https://mediabooth.com.au"
                  alt="Media Booth Australia"
                >
                  Media Booth Australia
                </Link>
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Footer;
