import React from "react";
import { BrowserRouter as Router, Route, Switch, useLocation} from "react-router-dom";


import Header from './components/header'
import Footer from './components/footer'
import Error404 from './components/error404'
import Home from './components/home'
import Locations from './components/locations'
import Profile from './components/profile'
import Advertise from './components/content/advertise'
import AboutUs from './components/content/aboutUs'
import AdvertiseSignUp from './components/content/advertiseSignUp'
import Escorts from "./components/content/escorts"
import Contact from "./components/content/contact"
import Categories from "./components/content/categories"
import Member from "./components/member/member"
import MemberActive from "./components/member/memberActive"
import MemberProfile from "./components/member/memberProfile"
import ResetPassword from "./components/member/resetPassword"

import BookingTCs from "./components/content/legal/bookingTCs"
import ReactGA from 'react-ga';

import './App.css';

ReactGA.initialize('UA-188200005-2');

function usePageViews() {
  let location = useLocation();
  React.useEffect(() => {
    console.log("pageview", location.pathname);
    ReactGA.set({ page: location.pathname });
    ReactGA.pageview(location.pathname)

  }, [location]);
}

function AppSwitch() {
  usePageViews();
  return (<Switch>
    {/*Filters Location - Home */}
    <Route exact path="/australia/:austate/:city/:suburb" component={(props) => <Locations  {...props} />} />
    <Route exact path="/australia/:austate/:city" component={(props) => <Locations  {...props} />} />
    <Route exact path="/australia/:austate" component={(props) => <Locations  {...props} />} />
    <Route exact path="/australia" component={(props) => <Locations  {...props} />} />

    {/*Filters Categories */}
    <Route exact path="/escorts/" component={(props) => <Escorts {...props} />} />

    <Route exact path="/member/active/:id" component={() => <MemberActive />} />
    <Route exact path="/member/profile" component={() => <MemberProfile />} />
    <Route exact path="/member/" component={() => <Member />} />
    <Route exact path="/reset-password/:id" component={() => <ResetPassword />} />

    <Route exact path="/contact/" component={() => <Contact  />} />
    <Route exact path="/advertise/" component={() => <Advertise />} />
    <Route exact path="/advertise/signup" component={() => <AdvertiseSignUp />} />
    <Route exact path="/escort/profile/:nickname" component={ () => <Profile  />} />


    {/*Terms and Conditions*/}
    <Route exact path="/terms-and-conditions/booking" component={ () => <BookingTCs  />} />

    <Route exact path="/about-us" component={(props) => <AboutUs  {...props}/>} />

    <Route exact path="/" component={(props) => <Home  {...props}/>} />
    <Route path="*" component={ () => <Error404  />} />
  </Switch>)
}

function App() {
  return(<Router>
    <div className="App">
      <div id="canvas">
		    <div id="box_wrapper">
          <Header />
            <AppSwitch />
          <Footer/>
        </div>
      </div>
    </div>
    </Router>)
}

export default App;
