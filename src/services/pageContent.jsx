import http from "./httpServices";
import { apiUrl } from "../config.json";

const apiEndpoint = `${apiUrl}/pages`;

export async function fetchPage(slug) {
  return await http.get(`${apiEndpoint}?slug=${slug}`);
}

const pageContent = {
  fetchPage,
};
export default pageContent;
