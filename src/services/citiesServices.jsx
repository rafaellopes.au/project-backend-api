import http from "./httpServices";
import { apiUrl } from "../config.json";

const apiEndpoint = `${apiUrl}/cities`;

export async function fetchCities() {
  return await http.get(`${apiEndpoint}`);
}

export async function fetchCLocation() {
  return await http.get(`${apiEndpoint}/loc`);
}

const citiesServices = {
  fetchCities,
  fetchCLocation,
};
export default citiesServices;
