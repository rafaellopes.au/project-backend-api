import http from "./httpServices";
import { apiUrl } from "../config.json";

const apiEndpoint = `${apiUrl}/options`;

export async function fetchOptions() {
  return await http.get(`${apiEndpoint}`);
}

const optionServices = { fetchOptions };

export default optionServices;
