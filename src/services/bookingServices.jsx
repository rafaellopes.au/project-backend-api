import http from "./httpServices";
import { adminApiUrl } from "../config.json";
import { fnEncrypt } from "../components/common/encrypt";

const apiEndpoint = `${adminApiUrl}/booking`;

export async function verify(mobile) {
  console.log(mobile);
  console.log(fnEncrypt(mobile));
  return await http.post(`${apiEndpoint}/verify`, {
    mobile: fnEncrypt(mobile),
  });
}

export async function confirmBooking(data) {
  let newData = data;
  newData.mobile = fnEncrypt(newData.mobile);
  newData.code = fnEncrypt(newData.code);
  return await http.post(`${apiEndpoint}/addnew`, newData);
}

export async function agenda(user_id, date) {
  return await http.get(`${apiEndpoint}/${user_id}/${date}`);
}

const bookingServices = {
  verify,
  confirmBooking,
  agenda,
};
export default bookingServices;
