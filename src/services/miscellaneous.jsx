import http from "./httpServices";
import { apiUrl } from "../config.json";

const apiEndpoint = `${apiUrl}/miscellaneous`;

export async function sendEmail(message, subject) {
  return await http.post(`${apiEndpoint}/sendmail`, { message, subject });
}

const miscellaneous = { sendEmail };

export default miscellaneous;
