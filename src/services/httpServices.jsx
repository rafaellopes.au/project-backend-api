import axios from "axios";

//axios.defaults.headers.common["x-token"] = "123";
axios.interceptors.response.use(null, (error) => {
  if (
    error.respons &&
    error.response.status > 400 &&
    error.response.status < 500
  ) {
    console.log("Logging the error", error);
  }

  return Promise.reject(error);
});

function setJwt(jwt) {
  axios.defaults.headers.common["x-auth-token"] = jwt;
}

const httpServices = {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  delete: axios.delete,
  setJwt,
};

export default httpServices;
