import http from "./httpServices";
import { adminApiUrl } from "../config.json";

const apiEndpoint = `${adminApiUrl}/users`;

export function register(user) {
  return http.post(apiEndpoint, user);
}

export default register;
