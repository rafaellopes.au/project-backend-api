import http from "./httpServices";
import { apiUrl } from "../config.json";
import jwtDecode from "jwt-decode";

const apiEndpoint = `${apiUrl}/members`;
http.setJwt(localStorage.getItem("tokenKey"));

export function register(data) {
  return http.post(apiEndpoint, data);
}

export function auth(data) {
  return http.post(apiEndpoint + "/auth", data);
}

export function active(id) {
  return http.post(apiEndpoint + "/active/" + id);
}

export function fetchMember() {
  return http.get(apiEndpoint);
}

export function getCurrentMember() {
  try {
    const jwt = localStorage.getItem("tokenKey");
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}

export function fetchMemberFavourites() {
  return http.get(apiEndpoint + "/favourite/list");
}

export function updateFavourite(profile_id) {
  return http.get(apiEndpoint + "/favourite/addremove/" + profile_id);
}

export function updateProfile(data) {
  return http.put(apiEndpoint, data);
}

export function updatePassword(data) {
  return http.put(apiEndpoint + "/password", data);
}

export function requestPasswordRestore(data) {
  return http.post(apiEndpoint + "/passrecovery", data);
}

export function resetPassword(data) {
  return http.post(apiEndpoint + "/resetpassword", data);
}

const memberService = {
  register,
  auth,
  getCurrentMember,
  fetchMemberFavourites,
  updateFavourite,
  updateProfile,
  updatePassword,
  requestPasswordRestore,
};

export default memberService;
